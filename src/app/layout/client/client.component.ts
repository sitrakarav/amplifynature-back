import { Component, OnInit } from '@angular/core';
import { TraductionService } from '../../shared/services/traduction.service';
import { ClientService } from '../../shared/services/client.service';
import { Client } from '../../shared/models/client.model';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {


	client:Client = new Client();
	clients:Client[];
	loading:Boolean = false;
	loadingPng:Boolean = false;
	cur:string;

  	traduction:any = {};
  	constructor(
  		public clientService:ClientService,
    	public tradService:TraductionService
  		) { }

	ngOnInit() {
		let self = this;
  		this.loading = true;

	    this.tradService.getAll().on('value', function(data){
	            data.forEach(function(child){
	              self.traduction[child.val().ref] = child.val();
	            });
	            console.log("traduction %o", self.traduction);
	      })

	  	this.clientService.list().subscribe(function(data){
	     
	                
	                self.loading = false;
	                let clients = data.map(a => ({ key: a.key, ...a.payload.val() }));
	                self.clients = clients.map((a:Client) => new Client().deserialize(a));
	               	console.log("clients : %o", self.clients.filter(e=> e.mail === "jordi.surkin@iucn.org"));
	               	for(let i in self.clients){
	               		self.setClient(self.clients[i]);
	               		break;
	               	}
		})
	}

	setClient(client:Client){
		this.client = client;
		this.loadingPng = true;
		setTimeout(function(){
			this.loadingPng = false;
		}.bind(this), 1000);
	}

	fr(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['fr'] : mot;
    }

    en(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['en'] : mot;
    }

}
