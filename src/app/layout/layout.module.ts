import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxLoadingModule } from 'ngx-loading';
import { ReactiveFormsModule } from '@angular/forms';
import {DpDatePickerModule} from 'ng2-date-picker';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { DropFileComponent } from './drop-file/drop-file.component';
import { ClientComponent } from './client/client.component';
import { EnjeuComponent } from './enjeu/enjeu.component';
import { PhraseComponent } from './phrase/phrase.component';
import { FileUploadModule } from 'ng2-file-upload';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { environment } from '../../environments/environment';
import { HomeComponent } from './home/home.component';
import { FormsModule} from '@angular/forms';
import { SafeHtmlPipe} from "../safeHtml.pipe";

import { PhrasesEnjeuComponent } from './phrases-enjeu/phrases-enjeu.component'

import { GenerateCsvComponent } from './generate-csv/generate-csv.component';
import { StatistiqueComponent } from './statistique/statistique.component'

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        FileUploadModule,
        NgbModule,
        NgxLoadingModule,
        FormsModule,
        AngularFireModule.initializeApp(environment.firebase), // imports firebase/app needed for everything
	    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
	    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
	    AngularFireStorageModule,
        ReactiveFormsModule,
        DpDatePickerModule

    ],

    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, DropFileComponent, ClientComponent, EnjeuComponent, PhraseComponent, HomeComponent, SafeHtmlPipe, PhrasesEnjeuComponent, GenerateCsvComponent, StatistiqueComponent]

})
export class LayoutModule {}
