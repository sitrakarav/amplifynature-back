import { Component, OnInit, ViewChild } from '@angular/core';
import { PhraseService } from '../../shared/services/phrase.service';
import { EnjeuService } from '../../shared/services/enjeu.service';
import { CsvPhrase } from '../../shared/models/csv-phrase.model';
import { Enjeu,PerfArray } from '../../shared/models/enjeu.model';
import { Phrase } from '../../shared/models/phrase.model';
import { NgbPanelChangeEvent, NgbAccordion } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireDatabase } from '@angular/fire/database';

import * as $ from 'jquery';


export class Sat{
  min:number;
  max:number;
  deserialize(input: any) {
      Object.assign(this, input);
      return this;
  }
}

export class SatWords{
  fr:string[];
  en:string[];
  deserialize(input: any) {
      Object.assign(this, input);
      return this;
  }
}

@Component({
  selector: 'app-phrase',
  templateUrl: './phrase.component.html',
  styleUrls: ['./phrase.component.scss']
})

export class PhraseComponent implements OnInit {

  @ViewChild('acc') accordion: NgbAccordion;

  csvPhrases:CsvPhrase[] = [];
  allCsvPhrases:CsvPhrase[] = [];
  phrases:Phrase[] = [];
  sat:Sat[] = [];
  satPhrases:{} = {};
  satWords:SatWords = new SatWords;
  prevCsv:any;
  curCsv:any;
  curPhrase:any;
  prevPhrase:any;
  seuil:number =0;
  perf:number = 0;
  perfSat:number = 0;
  perfs:Array<number>=[];
  loading:Boolean = false;
  enjeux:Enjeu[];
  enjeu2:Enjeu;
  enjeu3:Enjeu;
  threeMaxEnjeuPerf:Array<PerfArray>;
  firstLoading:boolean = true;

  textLoading:string ="Chargement...";
  constructor(
    public phraseService:PhraseService,
    public enjeuService:EnjeuService,
    private db: AngularFireDatabase,

    ) { }



  ngOnInit() {
    let self = this;
    self.loading = true;
    for(let i of [1,2,3,4]){
      this.sat[i] = new Sat;
      this.sat[i].min= i;
      this.sat[i].max= i+1;
    }

    this.db.database.ref("seuilSat").on("value", (seuilSat) => {
      if(seuilSat.val()){
        seuilSat.forEach((seuilSat)=>{
          this.sat[parseInt(seuilSat.key)] = new Sat().deserialize(seuilSat.val());
        });
       
      }
    });

    this.db.database.ref("satPhrases").on("value", (satPhrases) => {
      if(satPhrases.val()){
        self.satPhrases = satPhrases.val();
      
      }
    });

    
    this.phraseService.getWithPhrases().once("value", function(data){

    
 

      let enjeuRef = self.enjeuService.getAll().subscribe(function(data2){
                    enjeuRef.unsubscribe();

                    self.db.object("sat").snapshotChanges().subscribe((satData) =>{
                            let satWords =  satData.payload.val();
                            console.log("SAT WORDS %o", self.satWords);
                            self.satWords = new SatWords().deserialize(satWords);
                            console.log("SAT WORDS %o", self.satWords);
                            let hidden = document.getElementsByClassName('bg-secondary') ;
                            for (let i = 0; i < hidden.length; ++i) {
                              (hidden[i] as HTMLElement).style.display = "none";
                            }
                            
                            let enjeux = data2.map(a => ({ key: a.key, ...a.payload.val() }));
                            self.enjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));
                            
                                

                            self.phraseService.getSeuil().once("value",function(seuil){
                        
                              if(seuil.val()){
                                self.seuil = parseInt(seuil.val().toString());
                              }


                                let csvPhrases =  [];
             
                                csvPhrases = data.val();
                                self.csvPhrases = [];
                                for(let i in csvPhrases){
                                  self.csvPhrases.push(csvPhrases[i])
                                }  
                              

                             

                                let iter1 = 1;
                               
                                for(let i in self.csvPhrases){

                                  if(!self.csvPhrases[i].valid || iter1 == self.csvPhrases.length){

                                    self.csvPhrases[i].active = true;
                                    self.curCsv = i;
                                    
                                    // console.log("le csv phrase %o", self.csvPhrases[i])
                                    // console.log("les phases dans le csv %o", self.csvPhrases[i].phrases);

                                    let csvPhrase = self.csvPhrases[i]
                                    
                                    let phraseRef = self.phraseService.getPhrasesByCsv(csvPhrase.nom).subscribe(function(phrasesSnap){
                                        phraseRef.unsubscribe();
                                        console.log('ATO NDRAY')
                                        self.csvPhrases[i].phrases = [];
                                        let phrases = phrasesSnap.payload.val();
                                        // console.log("phrases getted %o", phrases);
                                        for(let p in phrases){
                                          let phrase = new Phrase().deserialize(phrases[p]);
                                          phrase.key = p;
                                        
                                          let perf = self.getMaxPerfPhrase(phrase).perf;
                                          if(perf >= self.seuil){
                                            self.csvPhrases[i].phrases.push(phrase);
                                           
                                          } 
                                        }



                                     
                                        
                                         // console.log("les phases dans le csv %o", self.csvPhrases[i].phrases);

                                        let iter = 1;
                                        
                                        for(let j in self.csvPhrases[i].phrases){
                                            // console.log("check valid %o", self.csvPhrases[i].phrases[j].valid);
                                            if(!self.csvPhrases[i].phrases[j].valid || iter == self.csvPhrases[i].phrases.length){
                                              self.curPhrase = j;
                             
                                              self.loading = false;
                                             
                                              self.getPerf();
                                              self.getNMaxPerfArray();

                                              break;
                                            }
                                            iter+=1;
                                        }

                                    }); 

                                          
                                    
                                      break;
                                  
                                  }
                                  iter1+=1;
                                }
                                

                                
                                   
                                
                                

                            });

                    })
                            



      });
                    

                    

          

          


 
      
    });

    

    
  }

  ngAfterViewInit(){
    let self = this;
    // alert('INIT');
      
      // console.log((document.getElementsByClassName('bg-secondary')[0] as HTMLElement));
      let iter1 = 1;
      for(let i in self.csvPhrases){
        if(!self.csvPhrases[i].valid  || iter1 == self.csvPhrases.length){

          (document.getElementById(self.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#ffc107';
          break;
        }
        iter1 +=1;
      }
    // this.getPerf();
  }



  next(){
    // console.log("this.csvPhrases[this.curCsv].phrases %o",this.csvPhrases[this.curCsv].phrases);
    window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
    this.loading = true;
    let self = this;
    // console.log("curPhrase : %o", self.curPhrase);
    let phrase = self.csvPhrases[self.curCsv].phrases[self.curPhrase];
    self.csvPhrases[self.curCsv].phrases[self.curPhrase].valid = true;
    self.csvPhrases[self.curCsv].phrases[self.curPhrase].compteur_perf = this.perf.toString();
    // console.log("phrase here %o",self.csvPhrases[self.curCsv].phrases[self.curPhrase]);
    let csvPhrase = self.csvPhrases[self.curCsv];
    for(let i in this.enjeux){
            if(this.enjeux[i].key == phrase.enjeu){
              phrase = this.phraseEnjeu(phrase, this.enjeux[i]);
              break;
            }
            
     }
    // console.log("satisfaction %o",this.csvPhrases[this.curCsv],self.csvPhrases[self.curCsv].phrases[self.curPhrase].satisfaction);
    let id = Number(phrase.num.match(/\d+/)[0]) -1;
 
    this.phraseService.update(this.csvPhrases[this.curCsv],self.csvPhrases[self.curCsv].phrases[self.curPhrase], phrase.key, this.enjeux);
    // .then(function(){
      self.loading = false;
      

      for(let i in self.csvPhrases){
          if(self.csvPhrases[i].nom == csvPhrase.nom && !self.csvPhrases[i].valid){
            self.csvPhrases[i].active = true;
            (document.getElementById(self.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#ffc107';
            self.curCsv = i;
            let iter = 1;
            for(let j in self.csvPhrases[i].phrases){
              if(!self.csvPhrases[i].phrases[j].valid){
                self.curPhrase = j;
                

                break;
              }
           
              if(iter == self.csvPhrases[i].phrases.length){
               
                  self.csvPhrases[i].valid = true;
                  self.phraseService.validate(self.csvPhrases[i])

                  self.next();
                  break;
              }
              iter+=1;
            }
            break;
            
          }else{
            (document.getElementById(self.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#b0bec5';
          }
        }
      self.getPerf();
    // })
      
  }

  saveSatSeuil(){
    let ok = true;
    // check every sat
    for(let i of [1,2,3,4]){

      if(this.sat[i].min > this.sat[i].max){
        // console.log("error 1 sat %o", i)
        // console.log("this.sat[i].min %o", this.sat[i].min);
        // console.log("this.sat[i].max %o", this.sat[i].max);
        ok = false;
        break;
      }
      // check beetween
      if(i>1 && i<4){
        if(this.sat[i-1].max > this.sat[i].min){
          // console.log("error 2 sat %o", i)

          ok = false;
          break;
        }
        if(this.sat[i].max > this.sat[i+1].min){
          // console.log("error 3 sat %o", i);
          // console.log("this.sat[i].max %o", this.sat[i].max);
          // console.log("this.sat[i+1].min %o", this.sat[i+1].min);
          
          // console.log(this.sat[i].max > this.sat[i+1].min);
          ok = false;
          break;
        }
      }
    }
    if(ok){
      this.loading = true;
      this.db.database.ref("seuilSat").set(this.sat).then(()=>{
        this.seuilSatChange();
      });
    }else{
      alert("Valeurs des seuils non valides");
    }

  }

  seuilSatChange(){
    let self = this;
    this.loading = true;
    let prom = new Promise((resolve, reject) => { 
      self.csvPhrases.forEach(function(csvPhrase, index,array){
        


            let ref = self.phraseService.getPhrasesByCsv(csvPhrase.nom).subscribe(function(phrasesSnap){
                                      ref.unsubscribe()
                                    
                                      let phrases = phrasesSnap.payload.val();
                                      let phrasesCsv:Phrase[] = [];
                                      for(let p in phrases){
                                        let phrase = new Phrase().deserialize(phrases[p]);
                                        phrase.key = p;
       
                                        
                                        phrase.satisfaction =  self.computeSatPerf(phrase).sat;
                                        phrasesCsv.push(phrase);

                                        
                                      }
                                     
                                      self.db.database.ref("phrases_csv/"+csvPhrase.nom).set(phrasesCsv).then(()=>{
                                        if(index === array.length -1) resolve();
                                      });
                                     
                                      

            });
          


        })
      });

    prom.then(() =>{
        self.loading = false;

    });

  }

  onSeuilChange(e:any){
    let self = this;

    this.loading = true;
    if(this.seuil){
      self.phraseService.setEnjeux(self.enjeux);
      // for all csv, get extrait
      self.satPhrases = [];
      this.db.database.ref("enjeux_extraits").remove();
      let prom = new Promise((resolve, reject) => { 
    

        for(let i = 0; i < self.csvPhrases.length; i++){
          let csvPhrase = self.csvPhrases[i];
          let index = i;


         
             

              let ref = self.phraseService.getPhrasesByCsv(csvPhrase.nom).subscribe(function(phrasesSnap){
                                      ref.unsubscribe();
                                      self.textLoading = "Traitement "+csvPhrase.nom+"...";
                                      csvPhrase.phrases = [];
                                      let phrases = phrasesSnap.payload.val();
                          
                                      for(let p in phrases){
                                        let phrase = new Phrase().deserialize(phrases[p]);
                                        phrase.key = p;

                                        phrase.satisfaction =  self.computeSatPerf(phrase).sat;


                                        let res = self.getMaxPerfPhrase(phrase);
                                        let perf = res.perf;
                                        phrase.compteur_perf = perf.toString();

                                        phrase.enjeu = res.enjeu ? res.enjeu.num : "";

                                        if(perf > self.seuil){

                                          phrase.valid = true;
                                        }else{
                                          phrase.valid = false;
                                        }
                                        
                                        csvPhrase.phrases.push(phrase);
                                        
                                        
                                      }
                                      // console.log("set extraits csv %o", csvPhrase.nom);
                                     
                                      self.satPhrases = self.phraseService.setEnjeuxExtraits(csvPhrase, self.seuil, self.satPhrases);
                                      if(index === self.csvPhrases.length -1) resolve();
                                      

              });
          



        }  
            
        
      });
      prom.then(() =>{
          self.textLoading = "Chargement...";
          self.db.database.ref("satPhrases").set(self.satPhrases);
          let csvPhrase = self.csvPhrases[self.curCsv];
          let ref = self.phraseService.getPhrasesByCsv(csvPhrase.nom).subscribe(function(phrasesSnap){
            ref.unsubscribe();
                                    csvPhrase.phrases = [];
                                    let phrases = phrasesSnap.payload.val();
 
                                    for(let p in phrases){
                                      let phrase = new Phrase().deserialize(phrases[p]);
                                      phrase.key = p;
                                   
                                      let perf = self.getMaxPerfPhrase(phrase).perf;

                                      if(perf >= self.seuil) csvPhrase.phrases.push(phrase);
                                    }

              let k = self.curCsv;
              for(let j in self.csvPhrases[k].phrases){
                    if(!self.csvPhrases[k].phrases[j].valid){
                      self.curPhrase = j;
                      break;
                    }
                    self.curPhrase = j;
              }
              

              self.loading = false;
              self.getPerf();
              self.getNMaxPerfArray();
          });
        

          

      });
      // for(let csvPhrase of self.csvPhrases){
        
      //         self.phraseService.getPhrasesByCsv(csvPhrase).subscribe(function(phrasesSnap){
      //                                 csvPhrase.phrases = [];
      //                                 let phrases = phrasesSnap.payload.val();
                                       
      //                                 for(let p in phrases){
      //                                   let phrase = new Phrase().deserialize(phrases[p]);
       
      //                                   csvPhrase.phrases.push(phrase);
                                        
      //                                 }
      //                                 console.log("set extraits csv %o", csvPhrase.nom)

      //                                 self.phraseService.setEnjeuxExtraits(csvPhrase, self.seuil);

      //         });
      // }
      this.phraseService.setSeuil(this.seuil).then(function(){
        console.log('FINISHED');
        // window.location.reload();
        // setTimeout(function(){
            // self.csvPhrases[self.curCsv].phrases = self.allCsvPhrases[self.curCsv].phrases.filter(function(value:Phrase, index:number, array:Phrase[]){
            //                         let perf = self.getMaxPerfPhrase(value).perf;

            //                         return perf >= self.seuil;

            // });
            // self.loading = false;
        // }.bind(this), 1000);
      });

        
          
    
    }
  }

  previous(){
    window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
    this.loading = true;
    let prevPhrase;
    for(let i in this.csvPhrases[this.curCsv].phrases){
      if(this.curPhrase == i){
        this.curPhrase = prevPhrase;
      }
      prevPhrase = i;
    }
    this.loading = false;
    
    this.getPerf();
  }

  setEnjeu(enjeu){
    
    console.log("set enjeu %o",this.csvPhrases[this.curCsv].phrases[this.curPhrase] );
    if(enjeu != 'none'){
      let phrase = this.csvPhrases[this.curCsv].phrases[this.curPhrase];
      this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu = enjeu.num;
      this.perf = this.computePerf(this.csvPhrases[this.curCsv].phrases[this.curPhrase], enjeu);
      phrase = this.phraseEnjeu(phrase, enjeu);
    }else{
      this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu = enjeu;
      this.perf = 0;
    }
    // console.log("AFTER setTING enjeu %o",this.csvPhrases[this.curCsv].phrases[this.curPhrase] );

    
    // console.log("perf %o", this.perf);
  }

  setSatisfaction(i){
    this.csvPhrases[this.curCsv].phrases[this.curPhrase].satisfaction = i;
    this.next();
  }

  setPhrase(i, j){
    // let j = p.num.match(/\d+/)[0];
    this.curCsv = i;
    this.curPhrase = j;
    // console.log("Phrase num %o : %o", this.curPhrase, this.csvPhrases[i].phrases[j]);

    for( let k in this.csvPhrases){
      if(k == i){
        (document.getElementById(this.csvPhrases[k].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#ffc107';
      }else{
        (document.getElementById(this.csvPhrases[k].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#b0bec5';
      }
    }
    let phrase = this.csvPhrases[this.curCsv].phrases[this.curPhrase];

    // this.getPerf();
    let res = this.getMaxPerfPhrase(phrase);
    this.perfSat = res.perfSat;

    if(!phrase.enjeu || phrase.enjeu == ""){
        // console.log("no enjeu");
       
       this.perf = res.perf;
      
       phrase = this.phraseEnjeu(phrase, res.enjeu);
       // this.setEnjeu(res.enjeu);
    }else{
        // console.log("enjeu phrase %o",phrase.enjeu);
       let enjeu = this.getEnjeu(phrase.enjeu);
       this.setEnjeu(enjeu);

       for(let i in this.enjeux){
            if(this.enjeux[i].key == phrase.enjeu){
         
              this.setEnjeu(this.enjeux[i]);
              this.perf = this.computePerf(this.csvPhrases[this.curCsv].phrases[this.curPhrase], this.enjeux[i]);
              phrase = this.phraseEnjeu(phrase, this.enjeux[i]);
              break;
            }
            
         }
    }
   

    this.getNMaxPerfArray();
  }
  // reachSeuil(phrase:Phrase){
  //   console.log(this.curPhrase);
  //   console.log("tous les enjeux :",this.enjeux);
  //   return true;
  // }
  getPerf(){
    // console.log("getting perf");
   if(this.csvPhrases[this.curCsv].phrases[this.curPhrase]){
      let phrase = this.csvPhrases[this.curCsv].phrases[this.curPhrase];
      let res = this.getMaxPerfPhrase(phrase);
      this.perfSat = res.perfSat;
        if(this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu == "" ){
          
          
          this.perf = res.perf;
          
          let enjeu = res.enjeu;
          phrase = this.phraseEnjeu(phrase, enjeu);

        }else{
   
          for(let i in this.enjeux){

            // console.log(this.enjeux[i].key)
            // console.log(this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu)
            if("#"+this.enjeux[i].key == this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu){
              // console.log('defening enjeu %o',this.enjeux[i]);
              // this.setEnjeu(this.enjeux[i]);
              let phrase = this.csvPhrases[this.curCsv].phrases[this.curPhrase]
              this.perf = this.computePerf(this.csvPhrases[this.curCsv].phrases[this.curPhrase], this.enjeux[i]);

              phrase = this.phraseEnjeu(phrase, this.enjeux[i]);
              break;
            }
            
          }
          
        }
   }
        
  }

  phraseEnjeu(phrase:Phrase, enjeu:Enjeu){
    if(phrase){
        let phrase_fr = " "+phrase.phrase_fr.toLowerCase()+" ";
        let phrase_en = " "+phrase.phrase_en.toLowerCase()+" ";
        
        phrase.phrase_fr_html = phrase_fr;
        phrase.phrase_en_html = phrase_en;


        // console.log(phrase_fr);
        // console.log(phrase_fr);
        // console.log("enjeu %o", enjeu);

        let perf_fr = 0;
        let perf_en = 0;
        //fr
        for(let i in enjeu.mots.fr){
          let mots = enjeu.mots.fr[i].mot.split(' ');

          let reg = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s");
          /*console.log("reg : ",reg);
          console.log("reg search : ",phrase_fr.search(reg))
          console.log("point : ",enjeu.mots.fr[i].point)*/
          if(phrase_fr.search(reg) > 0){
            // console.log("mot %o, point %o", enjeu.mots.fr[i].mot, enjeu.mots.fr[i].point);
            if(enjeu.mots.fr[i].point == 2){
              let re = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s","gi");
              phrase.phrase_fr_html = phrase.phrase_fr_html.replace(re, " <b> "+enjeu.mots.fr[i].mot+" </b> ");
              //console.log("deux points: ",phrase.phrase_fr_html)
            }

            if(enjeu.mots.fr[i].point == 6){
              let re = new RegExp('\\s' + enjeu.mots.fr[i].mot.toLowerCase() + '\\s','gi');
              console.log("old phrase : ",phrase.phrase_fr_html);
              console.log("regex phrase : ",re)
              phrase.phrase_fr_html = phrase.phrase_fr_html.replace(re, " <u><b> "+enjeu.mots.fr[i].mot+" </b></u> ");
              //phrase.phrase_fr_html = " <u><b> "+enjeu.mots.fr[i].mot+" </b></u> ";
              //console.log("six points: ",phrase.phrase_fr_html)
            }


          }
        }

        for(let i in this.satWords.fr){
          let mot = this.satWords.fr[i];

          let reg = new RegExp("\\s" + mot.toLowerCase() + "\\s");
          
   
          if(phrase_fr.search(reg) > 0){
            // console.log("mot %o, point %o", enjeu.mots.fr[i].mot, enjeu.mots.fr[i].point);
        
              let re = new RegExp("\\s" + mot.toLowerCase() + "\\s","gi");
              
              phrase.phrase_fr_html = phrase.phrase_fr_html.replace(re, " <span style='color:red;'> "+mot+" </span> ");
 
            
          }
        }


        //en
        for(let i in enjeu.mots.en){
          let reg = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s");
          if(phrase_en.search(reg) > 0){

            if(enjeu.mots.en[i].point == 2){
              let re = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s","gi");
              phrase.phrase_en_html = phrase.phrase_en_html.replace(re, " <b> "+enjeu.mots.en[i].mot+" </b> ");
            }
            if(enjeu.mots.en[i].point == 6){
              let re = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s","gi");
              phrase.phrase_en_html = phrase.phrase_en_html.replace(re, " <u><b> "+enjeu.mots.en[i].mot+" </b></u> ");
            }

          }
        }

        for(let i in this.satWords.en){
          let mot = this.satWords.en[i];

          let reg = new RegExp("\\s" + mot.toLowerCase() + "\\s");
          if(phrase_fr.search(reg) > 0){
           
              let re = new RegExp("\\s" + mot.toLowerCase() + "\\s","gi");
              phrase.phrase_en_html = phrase.phrase_en_html.replace(re, " <span style='color:red;'> "+mot+" </span> ");
            
          }
        }

        
    }
   
    return phrase;
  }

  getMaxPerf(){

    let enjeu:Enjeu;
    let perf = 0;
    for(let i in this.enjeux){
      this.setEnjeu(this.enjeux[i]);
      break;
    }
    for(let i in this.enjeux){
      enjeu = this.enjeux[i];
      let cur = this.computePerf(this.csvPhrases[this.curCsv].phrases[this.curPhrase], this.enjeux[i]);
      if(cur > perf){
        perf = cur;
      
        // console.log('enjeu %o', enjeu);
        this.setEnjeu(this.enjeux[i]);

      }
    }
    return perf;
  }

  getMaxPerfPhrase(phrase:Phrase){
    // console.log("getting max perf");
    
    let enjeu:Enjeu;
    let perf = 0;
    let perfSat = 0;
    
    for(let i in this.enjeux){
      
      let cur = this.computePerf(phrase, this.enjeux[i]);
  
      if(cur > perf){
        enjeu = this.enjeux[i];
        perf = cur;
      }
    }

    perfSat = this.computeSatPerf(phrase).points;

    return {"perf": perf, "enjeu": enjeu, "perfSat": perfSat};
  }

  getEnjeu(num: String){
    let res = this.enjeux.filter(function(value:Enjeu, index:number, array:Enjeu[]){
      return value.num == num;
    });
    return (res.length > 0 ) ? res[0] : null;

  }

  getEnjeuxPerfArray(phrase:Phrase){
    //initialisation
    let enjeu :Enjeu;
    let perf = 0;
    let perfArray:Array<PerfArray>=[];
    // for(let i in this.enjeux){
    //   this.setEnjeu(this.enjeux[i]);
    //   break;
    // }
    //calcule
    for(let i in this.enjeux){
      enjeu = this.enjeux[i];
      let cur = this.computePerf(this.csvPhrases[this.curCsv].phrases[this.curPhrase], this.enjeux[i]);
      
        perfArray.push({performance:cur,enjeu:enjeu});
      
        // this.setEnjeu(this.enjeux[i]);
     
    }
    console.log('perfArray', perfArray);
    return perfArray;
  }
  getNMaxPerfArray(n:number=3){
    let retour:Array<PerfArray>=[];
    let perfArray = this.getEnjeuxPerfArray(this.curPhrase);
    perfArray =this.tri(perfArray,'performance');
    for(var i=0;i<n;i++){
      retour.push(perfArray[i]);
    }
    console.log('perfArrayTri avec retour:', retour);

    this.threeMaxEnjeuPerf = retour;
    //this.perf= this.threeMaxEnjeuPerf[0].performance;
    //return retour;
  }
  tri(dataArray:any=[],byTemps_total='performance',n:number=null){
    if(n==null) n = dataArray.length;
    if(n==0){
      return dataArray;
    }
    else{
      let temp_max=0;
      let toShiftData=null;
      for(var i=0; i<n;i++){
        if(dataArray[i][byTemps_total] >= temp_max){
          temp_max = dataArray[i][byTemps_total]
          toShiftData= dataArray[i];
        }
      }
      toShiftData = dataArray.splice(dataArray.indexOf(toShiftData),1);
      dataArray.push(toShiftData[0]);
      return this.tri(dataArray,byTemps_total,n-1);
    }
    //console.log(dataArray);
  }
  computePerf(phrase:Phrase, enjeu:Enjeu){
    //mettre phrase en minuscule
    if(phrase){
        let phrase_fr = " "+phrase.phrase_fr.toLowerCase()+" ";
        let phrase_en = " "+phrase.phrase_en.toLowerCase()+" ";

        let perf_fr = 0;
        let perf_en = 0;
        //fr
        for(let i in enjeu.mots.fr){
          let reg = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s");
          if(phrase_fr.search(reg) > 0){
            perf_fr += enjeu.mots.fr[i].point;

          }
        }

        //en
        for(let i in enjeu.mots.en){
          let reg = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s");
          if(phrase_en.search(reg) > 0){
            perf_en += enjeu.mots.en[i].point;

          }
        }
        
        return (phrase.langue.toLowerCase() == "fr") ? perf_fr : perf_en;
    }else{
      return 0;
    }
        

  }

  computeSatPerf(phrase:Phrase){
    //mettre phrase en minuscule
    let res = 0
    if(phrase){
        let phrase_fr = " "+phrase.phrase_fr.toLowerCase()+" ";
        let phrase_en = " "+phrase.phrase_en.toLowerCase()+" ";

        let perf_fr = 0;
        let perf_en = 0;
        //fr
        for(let s in this.satWords.fr){
          let sat = this.satWords.fr[s];
        
          let reg = new RegExp("\\s" + sat.toLowerCase() + "\\s");
          if(phrase_fr.search(reg) > 0){
            perf_fr += 1;

          }
        }

        //en
        for(let s in this.satWords.en){
          let sat = this.satWords.en[s];
          let reg = new RegExp("\\s" + sat.toLowerCase() + "\\s");
          if(phrase_en.search(reg) > 0){
            perf_en += 1;

          }
        }

        res = (perf_fr > perf_en) ? perf_fr : perf_en;
        let satFound = false;
        for(let i of [1,2,3,4]){
          if(res >= this.sat[i].min && res < this.sat[i].max){
            phrase.satisfaction = i;
            satFound = true;
            break
          }
        }

        if(!satFound){
          phrase.satisfaction = 1;  
        }


    }

    return {points: res, sat: phrase.satisfaction};
        

  }

  public beforeChange($event: NgbPanelChangeEvent) {
      // console.log("panel id %o", $event.panelId);
      // console.log("cur csv  %o", this.curCsv);

      this.firstLoading = false;
      this.loading = true;
      for(let i in this.csvPhrases){
          if(this.csvPhrases[i].nom == $event.panelId){
            this.curCsv = i;
            (document.getElementById(this.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#ffc107';
          }else{

            (document.getElementById(this.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#b0bec5';
          }
      }
      let self = this;
      let csvPhrase = self.csvPhrases[self.curCsv];
      let ref = self.phraseService.getPhrasesByCsv(csvPhrase.nom).subscribe(function(phrasesSnap){
        ref.unsubscribe();
                                csvPhrase.phrases = [];
                                let phrases = phrasesSnap.payload.val();
                                // console.log("phrases getted %o", phrases);
                                for(let p in phrases){
                                  let phrase = new Phrase().deserialize(phrases[p]);
                                   phrase.key = p;
                                  let perf = self.getMaxPerfPhrase(phrase).perf;
                                  if(perf >= self.seuil) csvPhrase.phrases.push(phrase);
                                }
          // self.csvPhrases[self.curCsv].phrases = self.csvPhrases[self.curCsv].phrases.filter(function(value:Phrase, index:number, array:Phrase[]){

          //                         let perf = self.getMaxPerfPhrase(value).perf;

                                  
          //                         return perf >= self.seuil;

          // });
          let k = self.curCsv;
          for(let j in self.csvPhrases[k].phrases){
                if(!self.csvPhrases[k].phrases[j].valid){
                  console.log("non valid %o", self.csvPhrases[k].phrases[j]);
                  self.curPhrase = j;
                  break;
                }
                self.curPhrase = j;
          }
          

          self.loading = false;
          self.getPerf();
          self.getNMaxPerfArray();
      });

          
      // $event.preventDefault();
  }

  arrayNumber(n: number): any[] {
    return Array(n);
  }

  getSatMax(i:number){
    let max = 500;
    let min = 0;


    let res = [];
    for (var i = min; i <= max; i++) {
        res.push(i);
    }



    return res;
  }

  getSatMin(i:number){
    let max = 500;
    let min = 0;
     

    let res = [];
    for (var i = min; i <= max; i++) {
        res.push(i);
    }

    return res;
  }
  sansAccents(mot) {
   return mot.replace(/[ùûü]/g,"u").replace(/[îï]/g,"i").replace(/[àâä]/g,"a").replace(/[ôö]/g,"o").replace(/[éèêë]/g,"e").replace(/ç/g,"c");
  }

}
