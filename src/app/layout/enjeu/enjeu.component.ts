import { Component, OnInit } from '@angular/core';
import { EnjeuService } from '../../shared/services/enjeu.service';
import { TraductionService } from '../../shared/services/traduction.service';
import { Enjeu } from '../../shared/models/enjeu.model';

@Component({
  selector: 'app-enjeu',
  templateUrl: './enjeu.component.html',
  styleUrls: ['./enjeu.component.scss']
})
export class EnjeuComponent implements OnInit {

	enjeux:Enjeu[];
  enjeu:Enjeu;
  extraits:any[];
	enjeuExtrait:any;
	loading:Boolean = false;
	cur:string;

  traduction:any = {};

	constructor(
  	public enjeuService:EnjeuService,
    public tradService:TraductionService
  	) { }

  	ngOnInit() {
  		let self = this;
  		this.loading = true;
      

      

  		this.enjeuService.getEnjeux().once("value", (data) => {
         this.loading = false;
                
                let enjeux = [];
                data.forEach(function(child){
                  enjeux.push({ key: child.key, ...child.val() })
                })
                // let enjeux = data.map(a => ({ key: a.key, ...a.payload.val() }));
                self.enjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));
               	console.log("enjeux : %o", self.enjeux);
               	for(let i in self.enjeux){
               		self.setEnjeu(self.enjeux[i]);
               		break;
               	}


                 
      });

      self.enjeuService.getExtraits().once("value", (extraitsData) => {
                              self.extraits = extraitsData.val();

                              console.log("self.extraits %o", self.extraits );
          self.enjeuExtrait = {};
          for(let i in self.extraits){
            self.enjeuExtrait[i] = 0;
            for(let j in self.extraits[i]){
              self.enjeuExtrait[i] += Number(self.extraits[i][j]);
            }
          }

          console.log("27 %o ", self.extraits[27]);
                              
                              console.log("enjeux : %o", self.enjeux);
      });
                

      this.tradService.getAll().once('value', function(data){
            data.forEach(function(child){
              self.traduction[child.val().ref] = child.val();
            });
            console.log("traduction %o", self.traduction);
      })
      

  	}

  	setEnjeu(enjeu){
  		this.enjeu = new Enjeu().deserialize(enjeu);
  	}

    fr(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['fr'] : mot;
    }

    en(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['en'] : mot;
    }
}
