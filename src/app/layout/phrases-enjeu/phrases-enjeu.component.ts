import { Component, OnInit } from '@angular/core';
import { EnjeuService } from '../../shared/services/enjeu.service';
import { TraductionService } from '../../shared/services/traduction.service';
import { PhraseService } from '../../shared/services/phrase.service';
import { Enjeu } from '../../shared/models/enjeu.model';
import { Phrase } from '../../shared/models/phrase.model';
import { AngularFireDatabase } from '@angular/fire/database';



@Component({
  selector: 'app-phrases-enjeu',
  templateUrl: './phrases-enjeu.component.html',
  styleUrls: ['./phrases-enjeu.component.scss']
})
export class PhrasesEnjeuComponent implements OnInit {

	enjeux:Enjeu[];
  	enjeu:Enjeu;
  	extraits:any;
	enjeuExtrait:any = {};
	loading:Boolean = false;
  phrasesLoading:Boolean = false;
  textLoading:string = "Chargement";
	cur:string;
  phrases : any = {};
  lang:string = "fr";
  seuil:number = 0;

  	traduction:any = {};
  	constructor(
  		public enjeuService:EnjeuService,
    	public tradService:TraductionService,
      public phraseService:PhraseService,
      private db: AngularFireDatabase,
    ){ }

	ngOnInit() {
		let self = this;
  		this.loading = true;
      

      
    let enjeuRef = this.db.list("enjeu").snapshotChanges().subscribe((enjeuSnap) =>{
      let enjeux = [];
      enjeuRef.unsubscribe();
      enjeuSnap.forEach((enj) =>{
        enjeux.push({ key: enj.key, ...enj.payload.val() });
        
      });

      self.enjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));

      for(let i in self.enjeux){
        self.setEnjeu(self.enjeux[i]);
        break;
      }
    });
  		// this.enjeuService.getEnjeux().once("value", (data) => {
        	
                
                
    //             data.forEach(function(child){
    //               enjeux.push({ key: child.key, ...child.val() })
    //             })
    //             // let enjeux = data.map(a => ({ key: a.key, ...a.payload.val() }));
    //             self.enjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));
 
    //            	for(let i in self.enjeux){
    //            		self.setEnjeu(self.enjeux[i]);
    //            		break;
    //            	}


                 
    //   	});

      let seuilRef = this.db.object("seuil").snapshotChanges().subscribe((seuilData:any) =>{
        if(seuilData.payload.val()){
          this.seuil = parseInt(seuilData.payload.val());
        }
        let extraitsRef = this.db.object("enjeux_extraits").snapshotChanges().subscribe((extraitsSnap) =>{
          extraitsRef.unsubscribe();
          self.extraits = extraitsSnap.payload.val();
          self.enjeuExtrait = {};
          let iter = 1;
          for(let i in self.extraits){
              self.enjeuExtrait[i] = 0;

              if(iter == 1){
                this.getPhrases(self.extraits[i], "#"+i);
              }
              for(let j in self.extraits[i]){
                self.enjeuExtrait[i] += Number(self.extraits[i][j]);

              }
              iter +=1;
          }





        });

      })
        

      let tradRef = this.db.list("trad").snapshotChanges().subscribe((tradSnap) =>{
        tradRef.unsubscribe();
        self.loading = false;
        tradSnap.forEach((tradu:any) =>{
          self.traduction[tradu.payload.val().ref] = tradu.payload.val();
        })

      })
      // this.tradService.getAll().once('value', function(data){
      //       data.forEach(function(child){
      //         self.traduction[child.val().ref] = child.val();
      //       });
      // })


		
	}

  getPhrases(extraits, enjeu){
    this.phrasesLoading = true;
    let self = this;
    let prom = new Promise((resolve, reject) => {
        let csvEnjeu = [];
        for(let csv in extraits){
          csvEnjeu.push(csv);
        }  

            let phrases:Phrase[] = [];
                csvEnjeu.forEach((csv, index, array) =>{

                  let refPhrase = self.db.object("phrases_csv/"+csv).snapshotChanges().subscribe(function(phrasesSnap){

                    refPhrase.unsubscribe();
                    


                  phrasesSnap.payload.forEach(function(child){
                    
                    let phrase = new Phrase().deserialize(child.val());
                    phrase.langue = phrase.langue.toUpperCase();

                    if(parseInt(phrase.compteur_perf) > self.seuil &&  phrase.enjeu == enjeu){
                      phrase = self.phraseEnjeu(phrase, self.enjeux.find((elt) => elt.num == enjeu));
                      phrases.push(phrase);
                    }
                    
                    
                    return false;
                  });
                  
                 
                  if(index === array.length -1) {
                    self.phrases[enjeu] = phrases;
                    resolve();
                  }
           
                  });

                })
        
        
    });

    prom.then(() =>{
      console.log(this.phrases);
      this.phrasesLoading = false;
    })
      
  }

  phraseEnjeu(phrase:Phrase, enjeu:Enjeu){
 
    if(phrase){
        let phrase_fr = " "+phrase.phrase_fr.toLowerCase()+" ";
        let phrase_en = " "+phrase.phrase_en.toLowerCase()+" ";
        phrase.phrase_fr_html = phrase_fr;
        phrase.phrase_en_html = phrase_en;

        // console.log(phrase_fr);
        // console.log(phrase_fr);
        // console.log("enjeu %o", enjeu);

        let perf_fr = 0;
        let perf_en = 0;
        // console.log("START ########");
        //fr
        for(let i in enjeu.mots.fr){
          let mots = enjeu.mots.fr[i].mot.split(' ');

          let reg = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s");
          if(phrase_fr.search(reg) > 0){
            // console.log("mot FR %o, point %o, phrase %o", enjeu.mots.fr[i].mot, enjeu.mots.fr[i].point, phrase);
            if(enjeu.mots.fr[i].point == 2){
              let re = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s","gi");
              phrase.phrase_fr_html = phrase.phrase_fr_html.replace(re, " <b style='font-size:20px'> "+enjeu.mots.fr[i].mot+" </b> ");
            }

            if(enjeu.mots.fr[i].point == 6){
              let re = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s","gi");
              phrase.phrase_fr_html = phrase.phrase_fr_html.replace(re, " <u style='font-size:20px'><b> "+enjeu.mots.fr[i].mot+" </b></u> ");
            }


          }
        }

       


        //en
        for(let i in enjeu.mots.en){
          let reg = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s");
          if(phrase_en.search(reg) > 0){
            // console.log("mot ANGAIS %o, point %o, phrase %o", enjeu.mots.en[i].mot, enjeu.mots.en[i].point, phrase);
            if(enjeu.mots.en[i].point == 2){
              let re = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s","gi");
              phrase.phrase_en_html = phrase.phrase_en_html.replace(re, " <b style='font-size:20px'> "+enjeu.mots.en[i].mot+" </b> ");
            }
            if(enjeu.mots.en[i].point == 6){
              let re = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s","gi");
              phrase.phrase_en_html = phrase.phrase_en_html.replace(re, " <u style='font-size:20px'><b> "+enjeu.mots.en[i].mot+" </b></u> ");
            }

          }
        }

        

        
    }
   
    return phrase;
  }

	setEnjeu(enjeu){
    this.phrasesLoading = true;
  	this.enjeu = new Enjeu().deserialize(enjeu);
    if(this.extraits && (!this.phrases[this.enjeu.num] || this.phrases[this.enjeu.num]  == undefined)){
      // console.log("here %o", this.extraits[this.enjeu.key]);
        this.getPhrases(this.extraits[this.enjeu.key], this.enjeu.num);
    }else{
      this.phrasesLoading = false;
    }


  }

  changeLang(lang:string){
    this.lang = lang;
  }

  	fr(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['fr'] : mot;
    }

    en(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['en'] : mot;
    }

}
