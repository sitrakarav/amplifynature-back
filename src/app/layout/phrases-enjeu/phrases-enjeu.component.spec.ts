import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhrasesEnjeuComponent } from './phrases-enjeu.component';

describe('PhrasesEnjeuComponent', () => {
  let component: PhrasesEnjeuComponent;
  let fixture: ComponentFixture<PhrasesEnjeuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhrasesEnjeuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhrasesEnjeuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
