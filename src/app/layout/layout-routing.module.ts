import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { DropFileComponent } from './drop-file/drop-file.component';
import { ClientComponent } from './client/client.component';
import { EnjeuComponent } from './enjeu/enjeu.component';
import { PhraseComponent } from './phrase/phrase.component';
import { StatistiqueComponent } from './statistique/statistique.component';

import { PhrasesEnjeuComponent } from './phrases-enjeu/phrases-enjeu.component';

import { GenerateCsvComponent } from './generate-csv/generate-csv.component'


const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'drop-file', pathMatch: 'prefix' },
            { path: 'drop-file', component: DropFileComponent },
            { path: 'client', component: ClientComponent },
            { path: 'enjeu', component: EnjeuComponent },
            { path: 'phrase', component: PhraseComponent },
            { path: 'phrases-enjeu', component: PhrasesEnjeuComponent },
            { path: 'generate-csv', component: GenerateCsvComponent },
            { path: 'statistique', component: StatistiqueComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
