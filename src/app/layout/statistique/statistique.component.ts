import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../shared/services/client.service';
import { Client } from '../../shared/models/client.model';
import { Enjeu } from '../../shared/models/enjeu.model';
import { CsvPhrase } from '../../shared/models/csv-phrase.model';
import { AngularFireDatabase } from '@angular/fire/database';
import * as jQuery from "jquery";
import * as htmlToText from "html-to-text";
@Component({
  selector: 'app-statistique',
  templateUrl: './statistique.component.html',
  styleUrls: ['./statistique.component.scss']
})
export class StatistiqueComponent implements OnInit {

	flux:any[] = [];
	enjeux:Enjeu[];
	res:any[] = [];
	enjeuxExtraits:any[] = [];
	emetteur:string;
	loading:Boolean = false;
	clients:Client[] = [];
	yearsFrom:number[] = [];
	yearsTo:number[] = [];
	years:number[] = [];

	csvPhrases:CsvPhrase[] = [];
	csvPhrasesFiltered:CsvPhrase[] = [];
	waits:Boolean[];
	yearFrom: number;
	yearTo:number;
	result:Boolean = false;

	nbExtraits:number = 0;
	traduction:any = {};
	satTrad:string[] = ["Très Négatif", "Déceptif", "Encourageant", "Très Positif"];
	constructor(
		private clientService:ClientService,
		private db:AngularFireDatabase
	) {
	}

	ngOnInit() {
		this.loading = true;
		this.waits = [true, true, true, true];
		this.clientService.list().subscribe((data) => {
	      this.waits[0] = false;
	    	this.endLoading();

	      const clients = data.map(a => ({ key: a.key, ...a.payload.val() }));
	      this.clients = clients.map((a: Client) => new Client().deserialize(a));
	      let flux = [];
	      this.clients.map((a: Client) => a.flux ? a.flux.map(item => {
	        if (!flux.some(e => e === item.flux)) {
	          flux.push(item.flux);
	        }
	      }) : []);
	      this.flux = flux;
	    });

	    let refPhrases = this.db.list("phrases").snapshotChanges().subscribe( (data) => {
	    		refPhrases.unsubscribe();
	    		this.waits[1] = false;
	    		this.endLoading();

	    		this.csvPhrases = data.map( (a:any) => {

	                	return new CsvPhrase().deserialize(a.payload.val());
	            });


	    		this.csvPhrases = this.csvPhrases.map( (a:CsvPhrase) => {


	                		if(a.date.split('/').length == 3){
	                			if(parseInt(a.date.split('/')[2]) >= 2000)
	                			{
	                				this.years.push(parseInt(a.date.split('/')[2]));
	                			}
	                		}

	                	return a;
	            });

	    		// remove years duplicatates and sort asc
	            this.years = this.years.filter((item, pos) => {
				    return this.years.indexOf(item) == pos;
				}).sort((a, b) =>  a - b);
				this.yearsFrom = this.years;
				this.yearsTo = this.years;

		});

		let tradRef = this.db.list("trad").snapshotChanges().subscribe((tradSnap) =>{
	        tradRef.unsubscribe();
	        this.waits[2] = false;
	        this.loading = false;
	        tradSnap.forEach((tradu:any) =>{
	          this.traduction[tradu.payload.val().ref] = tradu.payload.val();
	        })

	    });

	    let enjeuRef = this.db.list("enjeu").snapshotChanges().subscribe((enjeuSnap) =>{
	      let enjeux = [];
	      this.waits[3] = false;
	      enjeuRef.unsubscribe();
	      enjeuSnap.forEach((enj) =>{
	        enjeux.push({ key: enj.key, ...enj.payload.val() });
	        
	      });

	      this.enjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));

	      
	    });



	}


	endLoading(){
		if(this.waits.every((w) => !w)){
			this.loading = false;
		}
	}

	yearChange(from:Boolean){
	
		if(from && this.yearFrom){
			this.yearsTo = this.years.filter((y) => y >= this.yearFrom);
		}

		if(!from && this.yearTo){
			this.yearsFrom = this.years.filter((y) => y <= this.yearTo);
		}
	}

	confirm(){
		this.loading = true;

		this.csvPhrasesFiltered = this.csvPhrases.filter((a) => {
			let res = false;
			if(this.cleanString(a.flux) == this.cleanString(this.emetteur)){

				res = true;
				return res;
			}
			return res;
		});

		if(this.yearFrom){
			this.csvPhrasesFiltered = this.csvPhrasesFiltered.filter((a) => {
							let res = false;
							if(a.date.split('/').length == 3){
									let date  = parseInt(a.date.split('/')[2]);
									if(date >= this.yearFrom){
										res = true;
									}
								}
								return res;
							})
			}

		if(this.yearTo){
			this.csvPhrasesFiltered = this.csvPhrasesFiltered.filter((a) => {
				let res = false;
				if(a.date.split('/').length == 3){
					let date  = parseInt(a.date.split('/')[2]);
					if(date <= this.yearTo){
						res = true;
					}
				}
				return res;
			})
		}


		this.res = [];
		let resAll = [];
		this.enjeuxExtraits = [];
		this.csvPhrasesFiltered.forEach((csv) =>{


				
				for(let index in csv.sat){
					if(parseInt(index) == 0 || !csv.sat[index]){
						continue;
					}
						let sat = csv.sat[index];
						
						if(this.res[index] == undefined){
							this.res[index] = [];

							
						} 
						if(Array.isArray(sat)){

							sat.forEach((s, index2) =>{
								if(this.res[index][index2] == undefined){
									this.res[index][index2] = s;
								}else{
									this.res[index][index2] += s;
								}
							});
						}

					
				}

				

				for(let index in csv.allSat){

					let sat = csv.allSat[index];
				
					if(resAll[index] == undefined){
						resAll[index] = [];

						
					} 
					if(Array.isArray(sat)){

						sat.forEach((s, index2) =>{
							if(resAll[index][index2] == undefined){
								resAll[index][index2] = s;
							}else{
								resAll[index][index2] += s;
							}
						});
					}
				}
					
				
			
				
		})


		// this.enjeuxExtraits = this.res.reduce(function(enjeuxExtraits, val){
		// 	if(enjeuxExtraits[val]){

		// 	}
		// 	return total + val.reduce(function(total2, extr){
		// 		return total2 + extr;
		// 	}, 0);
		// }, [])
		for(let index in this.res){
			let val = this.res[index].reduce(function(total, val){
				return total + val;
			}, 0);
			let sat = this.res[index].map((val2, key) => {
				return { "percent" : ((val2/val)*100).toFixed(2), 'key' : key, "sat" : this.fr(this.satTrad[key-1])};
			})
		

			this.enjeuxExtraits.push({'key': index, 'val' : val, 'sat' : sat});
		};
		this.enjeuxExtraits.sort((a, b) => b.val -a.val);
		// console.log(this.enjeuxExtraits);
		this.nbExtraits = resAll.reduce(function(total, val:[]){
			return total + val.reduce(function(total2, extr){
				return total2 + extr;
			}, 0);
		}, 0)

		this.enjeux.forEach((enj) =>{
			let key = enj.key;
			let exist = this.enjeuxExtraits.find((val) => val.key == enj.key);
			if(!exist){
				this.enjeuxExtraits.push({'key': key, 'val' : 0, 'sat' : []});
			}

		})
		this.result = true;
		this.loading = false;
	}

	download(){

		let elHtml = jQuery("#result").html();
		
		let anchor = document.createElement('a');
		let text = htmlToText.fromString(elHtml, {unorderedListItemPrefix: '-'});

		text = text.replace("-\n", "");
        let url = window.URL.createObjectURL(new Blob([text], {type: "text/plain;charset=utf-8"}));

        anchor.setAttribute('href', url);
        anchor.setAttribute('download', "statistique_"+this.emetteur+"_"+this.yearFrom+"_"+this.yearTo+".txt");
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
	}


	cleanString(s){
	  return s.toLowerCase().trim().replace(/[^a-z0-9]/ig,'');
	}

	fr(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['fr'] : mot;
    }

    en(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['en'] : mot;
    }

}
