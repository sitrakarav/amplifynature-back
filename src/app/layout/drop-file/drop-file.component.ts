import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../shared';
import { ClientService } from '../../shared/services/client.service';
import { TraductionService } from '../../shared/services/traduction.service';
import { EnjeuService } from '../../shared/services/enjeu.service';
import { SatService } from '../../shared/services/sat.service';
import { CouleurService } from '../../shared/services/couleur.service';
import { OrgaPngService } from '../../shared/services/orga-png.service';
import { ClientPngService } from '../../shared/services/client-png.service';
import { EnjeuPngService } from '../../shared/services/enjeu-png.service';
import { PhraseService } from '../../shared/services/phrase.service';
import { PdfService } from '../../shared/services/pdf.service';
import { Phrase } from '../../shared/models/phrase.model';
import { CsvPhrase } from '../../shared/models/csv-phrase.model';
import { DomSanitizer } from '@angular/platform-browser';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

export class Csv{
  name:string;
  url:string;
  delete:boolean = false;
}
const URL = '';
@Component({
  selector: 'app-drop-file',
  templateUrl: './drop-file.component.html',
  styleUrls: ['./drop-file.component.scss']
})


export class DropFileComponent implements OnInit {

	@ViewChild('content') public content;
	@ViewChild('pngError') public pngError;
	@ViewChild('pdfError') public pdfError;

	public client:FileUploader = new FileUploader({url: URL});
	public traduction:FileUploader = new FileUploader({url: URL});
  public enjeu:FileUploader = new FileUploader({url: URL});
	public sat:FileUploader = new FileUploader({url: URL});
	public couleur:FileUploader = new FileUploader({url: URL});
	public orga:FileUploader = new FileUploader({url: URL});
	public clientPng:FileUploader = new FileUploader({url: URL});
	public enjeuPng:FileUploader = new FileUploader({url: URL});
	public phrase:FileUploader = new FileUploader({url: URL});
	public pdf:FileUploader = new FileUploader({url: URL});

	public hasBaseDropZoneOver:any = {};

	errors:string[] = [];
	success:string[] = [];


	closeResult: string;

	csvClient:any= {};
	csvTraduction:any = {};
  csvEnjeu:any = {};
	csvSat:any = {};
	csvCouleur:any = {};
	listOrgaPng:any = [];
	listClientPng:any = [];
  listEnjeuPng:any = [];
  listPdf:any = [];
	listPhrase:any = [];

  phrases:any = [];

	loading:boolean=false;// afficher loader
	clientLoading:boolean=false;// afficher loader
	tradLoading:boolean=false;// afficher loader
	enjeuLoading:boolean=false;// afficher loader
  couleurLoading:boolean=false;// afficher loader
	satLoading:boolean=false;// afficher loader

	orgaPngLoading:boolean=false;// afficher loader
	clientPngLoading:boolean=false;// afficher loader
  enjeuPngLoading:boolean=false;// afficher loader
  phraseLoading:boolean=false;// afficher loader
	pdfLoading:boolean=false;// afficher loader

    constructor(
    	private modalService: NgbModal,
    	public clientService:ClientService,
    	public tradService:TraductionService,
      public enjeuService:EnjeuService,
    	public satService:SatService,
    	public couleurService:CouleurService,
    	public orgaPngService:OrgaPngService,
    	public clientPngService:ClientPngService,
    	public enjeuPngService:EnjeuPngService,
      public phraseService:PhraseService,
    	public pdfService:PdfService,
      private sanitizer:DomSanitizer,
      private db: AngularFireDatabase,
      private storage: AngularFireStorage,
    	) { }

  	ngOnInit() {
  		// console.log(this.clientService);
  		this.hasBaseDropZoneOver = {
  			'client': false,
  			'traduction': false,
        'enjeu': false,
  			'sat': false,
  			'couleur': false,
  			'clientPng': false,
  			'enjeuPng': false,
  			'phrase': false,
  			'pdf': false
  		}
  		let self = this;
  		this.loading = true;

  		this.clientService.getfile().subscribe(function(snapshot){
        if(snapshot.payload.val()){
          self.csvClient = snapshot.payload.val();
          //console.log("csv client %o",  self.csvClient);
        }

  		});
  		this.tradService.getfile().subscribe(function(snapshot){
  			//console.log("csv trad %o", snapshot.payload.val());
        if(snapshot.payload.val()){
          self.csvTraduction = snapshot.payload.val();
          self.csvTraduction.delete = false;
        }

  		});

  		this.enjeuService.getfile().subscribe(function(snapshot){
  			//console.log("csv enjeu %o", snapshot.payload.val());
        if(snapshot.payload.val()){
          self.csvEnjeu = snapshot.payload.val();
          self.csvEnjeu.delete = false;
        }

  		});

      this.satService.getfile().subscribe(function(snapshot){
        //console.log("csv sat %o", snapshot.payload.val());
        if(snapshot.payload.val()){
          self.csvSat = snapshot.payload.val();
          if(self.csvSat.url2 == undefined){
            self.storage.ref("csv_sat/"+self.csvSat.name).getDownloadURL().subscribe((url) =>{
              self.csvSat.url2 = url;
              //console.log("self.csvSat %o", self.csvSat);
            })
          }
          self.csvSat.delete = false;
        }

      });



  		this.couleurService.getfile().subscribe(function(snapshot){
  			//console.log("csv couleur %o", snapshot.payload.val());
        if(snapshot.payload.val()){
          self.csvCouleur = snapshot.payload.val();
          self.csvCouleur.delete = false;
        }


  		});

  		this.orgaPngService.list().subscribe(function(data){

  			self.listOrgaPng = data.map((item) => ({key:data.keys,delete:false, ...item.payload.val()}) );
  			//console.log("orga png %o", self.listOrgaPng);
  		});

  		this.clientPngService.list().subscribe(function(data){

  			self.listClientPng = data.map((item) => ({key:data.keys,delete:false, ...item.payload.val()}) );

  			//console.log("client png %o", self.listClientPng);
  		});

  		this.enjeuPngService.list().subscribe(function(data){

  			self.listEnjeuPng = data.map((item) => ({key:data.keys,delete:false, ...item.payload.val()}) );

  			//console.log("enjeu png %o", self.listEnjeuPng);
  		})


      this.phraseService.getAll().subscribe(function(data){
          self.phrases =  data.map((item) => ({key:data.keys,delete:false, ...item.payload.val()}) );

          // console.log('phrases %o', self.phrases);
          self.loading = false;
      });

      // this.phraseService.list().once('value', function(data){

      // // }) subscribe(function(data){

      //   self.listPhrase = [];
      //   data.forEach(function(childSnap){
      //     let item = childSnap.val();
      //     item.key = childSnap.key;
      //     item.delete = false;
      //     self.listPhrase.push(item);
      //   })

      //   self.loading = false;
      //   // for(let i in self.listPhrase){
      //   //   let csv = "";
      //   //   self.phraseService.getCsv(self.listPhrase[i].name.split('.')[0]).once('value', function(snapshot){
      //   //       if(snapshot.val()){
      //   //         csv += "data:text/csv;charset=utf-8,";
      //   //         let data = snapshot.val();
      //   //         // add headers
      //   //         csv +="num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y";
      //   //         csv += "\n";

      //   //         //add content
      //   //         for(let j in snapshot.val().phrases){
      //   //           let phrase = snapshot.val().phrases[j];
      //   //           // console.log("PHRASE CSV : %o", phrase);
      //   //           let row = ""+phrase.num+";"+phrase.langue+";"+phrase.compteur_perf+";"+phrase.enjeu.replace(/#/g, '')+";"+phrase.satisfaction+";"+data.nom+";"+data.nom_pdf+";"+phrase.phrase_fr.replace(/\r/g, '').replace(/\n/g, '')+";"+phrase.phrase_en.replace(/\r/g, '').replace(/\n/g, '')+";"+phrase.flux+";"+data.titre_fr+";"+data.titre_en+";"+data.auteur+";"+data.date+";"+data.pays+";"+data.long+";"+data.lat;
      //   //           // console.log('ROW %o', row);
      //   //           csv = csv + row;
      //   //           csv = csv + "\n";
      //   //           // console.log('CSV %o', csv);

      //   //         };

      //   //         self.listPhrase[i].csv  = encodeURI(csv);

      //   //       }
      //   //     });

      //   // }
      //   console.log("phrases %o", self.listPhrase);
      // })

      this.pdfService.list().subscribe(function(data){

        self.listPdf = data.map((item) => ({key:data.keys,delete:false, ...item.payload.val()}) );
        self.loading = false;
        //console.log("pdf %o", self.listPdf);
      })
  	}

    exportCsvPhrase(phrase){
      this.loading = true;
      //console.log("exportCsvPhrase %o", phrase);
      let self = this;
        let csv = "";
          this.phraseService.getCsv(phrase.name.split('.')[0]).on("value", function(snapshot){
              //console.log(snapshot.val());
              if(snapshot.val()){


                //add content
                let csvPhrase = new CsvPhrase().deserialize(snapshot.val());


                self.phraseService.getPhrasesByCsv(csvPhrase.nom).subscribe(function(phrasesSnap){
                    console.log("here");

                    let csvArray = [];
                    // csvArray.push("data:text/csv;charset=utf-8,");
                    csv += "data:text/csv;charset=utf-8,";
                    let data = snapshot.val();
                    // add headers
                    csv +="num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;nom_PDG;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y";
                    csv += "\n";

                    csvArray.push("num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;nom_PDG;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y");



                    let phrases:any = phrasesSnap.payload.val();
                    // console.log("PHRASEs : %o", phrases);

                    for(let j in phrases){
                      let phrase = phrases[j];
                      // console.log("PHRASE CSV : %o", phrase);
                      let row = ""+phrase.num+";"+phrase.langue+";"+phrase.compteur_perf+";"+phrase.enjeu.replace(/#/g, '')+";"+phrase.satisfaction+";"+data.nom+";"+data.nom_pdf+";"+data.nom_pdg+";"+phrase.phrase_fr.replace(/\r/g, '').replace(/\n/g, '')+";"+phrase.phrase_en.replace(/\r/g, '').replace(/\n/g, '')+";"+phrase.flux+";"+data.titre_fr+";"+data.titre_en+";"+data.auteur+";"+data.date+";"+data.pays+";"+data.long+";"+data.lat;
                      // console.log('ROW %o', row);
                      csv = csv + row;
                      csv = csv + "\n";
                      csvArray.push(row);


                    };



                    let csvContent = csvArray.join("\n");



                    self.loading = false;
                    let anchor = document.createElement('a');
                    console.log('CSV %o', csvContent);

                    let url = window.URL.createObjectURL(new Blob([csvContent], {type: "text/csv;charset=utf-8"}));

                    anchor.setAttribute('href', url);
                    anchor.setAttribute('download', phrase.name);
                    document.body.appendChild(anchor);
                    anchor.click();
                    document.body.removeChild(anchor);
                });

                // console.log(encodeURI(csv));
                // window.open(encodeURI(csv));

              }
            });
    }

    dataURLToBlob = function(dataURL)
    {
        let BASE64_MARKER = ";base64,";
        if (dataURL.indexOf(BASE64_MARKER) == -1)
        {
            let parts = dataURL.split(",");
            let contentType = parts[0].split(":")[1];
            let raw = decodeURIComponent(parts[1]);

            return new Blob([raw], {type: contentType});
        }

        let parts = dataURL.split(BASE64_MARKER);
        let contentType = parts[0].split(":")[1];
        let raw = window.atob(parts[1]);
        let rawLength = raw.length;

        let uInt8Array = new Uint8Array(rawLength);

        for (let i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }

  	export(){
  		this.errors = [];
  		this.success = [];
  		let self = this;

  		if(this.client.queue.length > 0){
  			this.loading = true;
  			this.clientLoading = true;
  			this.clientService.export(this.client.queue[0]['_file'], this.client.queue[0].file.name, this);
  		}
  		if(this.traduction.queue.length > 0){
  			this.loading = true;
  			this.tradLoading = true;

  			this.tradService.export(this.traduction.queue[0]['_file'], this.traduction.queue[0].file.name, this);
  		}
  		if(this.enjeu.queue.length > 0){
  			this.loading = true;
  			this.enjeuLoading = true;
  			this.enjeuService.export(this.enjeu.queue[0]['_file'], this.enjeu.queue[0].file.name, this);
  		}

      if(this.sat.queue.length > 0){
        this.loading = true;
        this.satLoading = true;
        this.satService.export(this.sat.queue[0]['_file'], this.sat.queue[0].file.name, this);
      }

  		if(this.couleur.queue.length > 0){
  			this.loading = true;
  			this.couleurLoading = true;
  			this.couleurService.export(this.couleur.queue[0]['_file'], this.couleur.queue[0].file.name, this);
  		}

  		if(this.orga.queue.length > 0){
  			this.loading = true;
  			this.orgaPngLoading = true;
  			this.orga.queue.forEach(function(value, index, array){
  				self.orgaPngService.export(value, self);
  			});
  		}

  		if(this.clientPng.queue.length > 0){

  			this.loading = true;
  			this.clientPng.queue.forEach(function(value, index, array){
  				//console.log("exporting %o", value);
  				self.clientPngService.export(value, self);
  			});
  		}

  		if(this.enjeuPng.queue.length > 0){

  			this.loading = true;
        for(let i in this.enjeuPng.queue){
          self.enjeuPngService.export(this.enjeuPng.queue[i], self);
        }

  		}

  		if(this.phrase.queue.length > 0){
  			this.loading = true;
        for(let i in this.phrase.queue){
          self.phraseService.export(this.phrase.queue[i], self);
        }

  		}

      if(this.pdf.queue.length > 0){

        // this.loading = true;
        this.pdf.queue.forEach(function(value, index, array){
          //console.log("exporting %o", value);
          self.pdfService.export(value, self);
        });
      }
  	}

  	remove(item){
  		item.delete = !item.delete;
  	}

  	removeAll(){
      this.errors = [];
      this.success = [];
      if(this.csvClient.delete){
        this.clientService.remove(this.csvClient, this);
      }
      if(this.csvTraduction.delete){
        this.tradService.remove(this.csvTraduction, this);
      }
      if(this.csvEnjeu.delete){
        this.enjeuService.remove(this.csvEnjeu, this);
      }

      if(this.csvSat.delete){
        this.satService.remove(this.csvSat, this);
      }
      if(this.csvCouleur.delete){
        this.couleurService.remove(this.csvCouleur, this);
      }
  		for(let i in this.listOrgaPng){
  			if(this.listOrgaPng[i].delete){
  				this.orgaPngService.remove(this.listOrgaPng[i], this);
  			}
  		}
  		for(let i in this.listClientPng){
  			if(this.listClientPng[i].delete){
  				this.clientPngService.remove(this.listClientPng[i], this);
  			}
  		}

  		for(let i in this.listEnjeuPng){
  			if(this.listEnjeuPng[i].delete){
  				this.enjeuPngService.remove(this.listEnjeuPng[i], this);
  			}
  		}

      for(let i in this.phrases){
        // console.log("delete %o", this.phrases[i]);
        if(this.phrases[i].delete){
          this.phraseService.remove(this.phrases[i], this);
        }
      }

      for(let i in this.listPdf){
        if(this.listPdf[i].delete){
          this.pdfService.remove(this.listPdf[i], this);
        }
      }
  	}

	public fileOverBase(e:any, zone:string):void {
	    this.hasBaseDropZoneOver[zone] = e;
	    // console.log(e);
	}


	onClientDrop(e:any):void{
		this.checkCsv(e, this.client);
		this.uniqueFile(e, this.client);
	}

	onTraductionDrop(e:any):void{
		this.checkCsv(e, this.traduction);
		this.uniqueFile(e, this.traduction);
	}

	onEnjeuDrop(e:any):void{
		this.checkCsv(e, this.enjeu);
		this.uniqueFile(e, this.enjeu);
	}

  onSatDrop(e:any):void{
    this.checkCsv(e, this.sat);
    this.uniqueFile(e, this.sat);
  }

	onCouleurDrop(e:any):void{
		this.checkCsv(e, this.couleur);
		this.uniqueFile(e, this.couleur);
	}

	onOrgaDrop(e:any):void{
		this.checkPng(e, this.orga);
	}

	onClientPngDrop(e:any):void{
		this.checkPng(e, this.clientPng);
	}

	onEnjeuPngDrop(e:any):void{
		this.checkPng(e, this.enjeuPng);
	}

	onPhraseDrop(e:any):void{
		this.checkCsv(e, this.phrase);
	}

	onPdfDrop(e:any):void{
		this.checkPdf(e, this.pdf);
	}

	removeFile(item:any, uploader:FileUploader){
		console.log("drop file %o", item);

		uploader.removeFromQueue(item);
	}


	public checkCsv(e:any, uploader:FileUploader):void {

		for (let i = 0; i < uploader.queue.length; ++i) {
			//alert(uploader.queue[i].file.type);
			if(uploader.queue[i].file.type != "text/csv" && uploader.queue[i].file.type != "application/vnd.ms-excel"){
				uploader.removeFromQueue(uploader.queue[i]);
				this.openModal(this.content);
				this.checkCsv(e, uploader);
			}

		}
	}

	public checkPng(e:any, uploader:FileUploader):void {
		for (let i = 0; i < uploader.queue.length; ++i) {
		  // alert(uploader.queue[i].file.type);
			if(uploader.queue[i].file.type != "image/png" && uploader.queue[i].file.type != "image/svg+xml" && uploader.queue[i].file.type != "image/jpeg"){
				this.openModal(this.pngError);
				uploader.removeFromQueue(uploader.queue[i]);

				this.checkPng(e, uploader);
			}

		}
	}

	public checkPdf(e:any, uploader:FileUploader):void {

		for (let i = 0; i < uploader.queue.length; ++i) {

			if(uploader.queue[i].file.type != "application/pdf" && uploader.queue[i].file.type != "image/png"){

				this.openModal(this.pdfError);
				uploader.removeFromQueue(uploader.queue[i]);

				this.checkPdf(e, uploader);
			}

		}
	}

	public uniqueFile(e:any, uploader:FileUploader):void{
		//console.log("first %o",uploader.queue);
		if(uploader.queue.length > 1){
			for (let i = 0; i < uploader.queue.length; ++i) {
				uploader.removeFromQueue(uploader.queue[i]);
			}
			//console.log("after %o",uploader.queue);
			if(uploader.queue.length > 1){
				this.uniqueFile(e, uploader);
			}

		}
	}


  	openModal(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    sanitize(url:string){

        return this.sanitizer.bypassSecurityTrustUrl(url);
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }



}
