import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { GenerateCsvService } from '../../shared/services/generate-csv.service';
import { ClientService } from '../../shared/services/client.service';
import { Client } from '../../shared/models/client.model';
import { PhraseService } from '../../shared/services/phrase.service';
import { Phrase } from '../../shared/models/phrase.model';
import { CsvPhrase } from '../../shared/models/csv-phrase.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePickerComponent} from 'ng2-date-picker';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FileUploader, FileItem } from 'ng2-file-upload';
@Component({
  selector: 'app-generate-csv',
  templateUrl: './generate-csv.component.html',
  styleUrls: ['./generate-csv.component.scss']
})
export class GenerateCsvComponent implements OnInit {
  @ViewChild('success') public success;
  filename: boolean = false; ;
  error = '';
  client: Client = new Client();
  clients: Client[];
  loading: Boolean = false;
  loadingPng: Boolean = false;
  flux = [];
  phrases = [];
  file = null;
  phrasesArray = null;

  formGroup: FormGroup;
  submitted = false;

  today = new Date(Date.now());
  maxMonth = this.today.getMonth() + 1;
  maxDate = this.today.getDate();
  maxYear = this.today.getFullYear();

  csv3Result: Boolean = true;
  txtFileResult: Boolean = false;
  txtError = null;

  closeResult: string;

  public hasBaseDropZoneOver:any = {};
  disabled = false;

  temp = [];

  public txt:FileUploader = new FileUploader({url: 'URL'});

  constructor(
    private modalService: NgbModal,
    private el: ElementRef,
    private renderer: Renderer2,
    private clientService: ClientService,
    private phraseService: PhraseService,
    private formBuilder: FormBuilder
    ) { }

  @ViewChild('txtInput') txtInput: ElementRef;

  ngOnInit() {
    const self = this;
    self.loading = true;

    for(let i = 1; i <= 9999; i++)
    {
      if(i<10)
      {
        self.temp.push('CSV3_000'+ i)
      }
      else if(i>= 10 && i<100)
      {
        self.temp.push('CSV3_00'+ i)
      }
      else if(i>= 100 && i<1000)
      {
        self.temp.push('CSV3_0'+ i)
      }
      else
      {
        self.temp.push("CSV3_"+i);
      }
    }

    this.hasBaseDropZoneOver = {
        'txt': false
      }

    self.formGroup = self.formBuilder.group({
        csv3: ['', Validators.required],
        titre: ['', Validators.required],
        gps_x: ['', Validators.required],
        gps_y: ['', Validators.required],
        langue: ['FR', Validators.required],
        //pays: ['', Validators.required],
        flux: ['', Validators.required],
        auteur: ['', Validators.required],
        selectedDate : ['', Validators.required]
    });

    this.clientService.list().subscribe(function(data) {
      self.loading = false;
      const clients = data.map(a => ({ key: a.key, ...a.payload.val() }));
      self.clients = clients.map((a: Client) => new Client().deserialize(a));
      let flux = [];
      self.clients.map((a: Client) => a.flux ? a.flux.map(item => {
        if (!flux.some(e => e === item.flux)) {
          flux.push(item.flux);
        }
      }) : []);
      self.flux = flux;
    });


    /*this.phraseService.list().once('value', function(data) {
      if (data.val()) {
        let index = 0;
        data.forEach((phrase) => {
          index++;
          self.phrases.push(new CsvPhrase().deserialize(phrase.val()).nom);
          self.temp = self.temp.filter(e => {
            return e !== phrase.val().nom;
          })
          self.loading = false;
        });
      }
    });*/
    this.phraseService.getAll().subscribe(result => {
      if (result) {
        let index = 0;
        result.forEach((phrase : any) => {
          let newPhrase = phrase.payload.key.toUpperCase();
          self.phrases.push(newPhrase);
          /*self.temp = self.temp.filter(e => {
            return e !== phrase.payload.key.toUpperCase()
          })*/

          //self.loading = false;
        });
        let newCsvList = [];

        newCsvList = self.temp.filter(e=> {
          return !self.phrases.includes(e);
        })
        if(newCsvList.length > 0)
        {
          console.log("length > 0")
          self.temp = newCsvList;
          self.loading = false;
        }
        
      }
    })
    //self.loading = false;
      }

  get f() {return this.formGroup.controls; }

  openFileBrowser() {
    if(this.txt.queue.length == 0)
    {
      this.txtInput.nativeElement.click();
    }

  }

  onFileChange($event) {
    this.filename = $event.target.files[0].name;
    if ($event.target.files[0].type !== 'text/plain') {
      this.txtFileResult = false;
      this.txtError = 'Type de fichier invalide';
    } else {
      this.txtFileResult = true;
      this.txtError = null;
    }
  }

  onSubmit() {

    const self = this;
    self.submitted = true;
    console.log(self.f.csv3.value)
    if (self.txtInput.nativeElement.files.length === 0 && self.txt.queue.length === 0) {
      this.txtFileResult = false;
      this.txtError = 'Importer fichier txt';
    }
    if (self.formGroup.invalid) {
      console.log("formgroup invalid")
        return;
    }
    if (!self.csv3Result) {
      console.log("csv3result invalid")
      return;
    }
    if (!self.txtFileResult) {
      console.log("txtfile invalid")
      return;
    }

    let csvPhrase = new CsvPhrase();
    let template = self.f.csv3.value.split('_');
    let csv3nom = template[1];

    csvPhrase.auteur = self.f.auteur.value;
    csvPhrase.date = (self.f.selectedDate.value.day < 10 ? "0" : "") + self.f.selectedDate.value.day + "/" + (self.f.selectedDate.value.month < 10 ? "0" : "") + self.f.selectedDate.value.month + "/" + self.f.selectedDate.value.year;
    csvPhrase.flux = self.f.flux.value;
    csvPhrase.lat = self.f.gps_x.value;
    csvPhrase.long = self.f.gps_y.value;
    csvPhrase.nom = 'CSV3_' + csv3nom;
    csvPhrase.nom_pdf = 'pdf3_' + csv3nom;
    csvPhrase.nom_pdg = 'pdg3_' + csv3nom;
    csvPhrase.pays = "FR";
    if (self.f.langue.value === 'FR') {
      csvPhrase.titre_fr = self.f.titre.value;
      csvPhrase.titre_en =  self.f.titre.value;
    } else {
      csvPhrase.titre_fr =  self.f.titre.value;
      csvPhrase.titre_en = self.f.titre.value;
    }
    console.log(csvPhrase);

    this.phraseService.add(csvPhrase);

    const reader = new FileReader();
    //reader.
    if(self.txtInput.nativeElement.files[0])
    {
      let tableau = [];
      reader.readAsText(self.txtInput.nativeElement.files[0]);
      reader.onload = function () {

        tableau = reader.result.toString().split('\n');
        tableau = tableau.filter(function(value, index, arr){
          return value != '';
        });
        tableau = tableau.map((e,index) => {
          let item = {
            compteur_perf : "0",
            csv_phrase_id : csvPhrase.nom,
            enjeu : "",
            flux: csvPhrase.flux,
            key : index,
            langue: self.f.langue.value,
            num : "Phrase"+index,
            // phrase_en : self.f.langue.value !== 'FR' ? e : '',
            // phrase_fr : self.f.langue.value === 'FR' ? e : '',
            phrase_en : e,
            phrase_fr : e,
            satisfaction : 0,
            valid: false
          }
          return item;
        })
        self.phraseService.addPhrases(csvPhrase,tableau);
        self.submitted = false;
        self.clear()
        self.openModal(self.success);
      }
    }
    if(self.txt.queue[0])
    {
      let tableau = [];
      console.log("file : ", self.txt.queue[0]);
      reader.readAsText(self.txt.queue[0]._file);
      reader.onload = function () {

        tableau = reader.result.toString().split('\n');
        tableau = tableau.filter(function(value, index, arr){
          return value != '';
        });

        tableau = tableau.map((e,index) => {
            let item = {
              compteur_perf : "0",
              csv_phrase_id : csvPhrase.nom,
              enjeu : "",
              flux: csvPhrase.flux,
              key : index,
              langue: self.f.langue.value,
              num : "Phrase"+index,
              // phrase_en : self.f.langue.value !== 'FR' ? e : '',
              // phrase_fr : self.f.langue.value === 'FR' ? e : '',
              phrase_en : e,
              phrase_fr : e,

              satisfaction : 0,
              valid: false
            }
            return item;
        })
        self.phraseService.addPhrases(csvPhrase,tableau);
        self.submitted = false;
        self.clear()
        self.openModal(self.success);
      }
    }
  }

  clear(){
    this.formGroup.reset();
    if(this.txt.queue[0])
    {
      this.txt.removeFromQueue(this.txt.queue[0]);
    }
    

  }

  get verifyCSV3() {
    const self = this;
    const csv3 = self.f.csv3.value;
    let result = true;
/*    if (csv3.length < 4) {
      result = false;
      self.csv3Result = result;
      return 'trop_court';
    } else if (parseInt(csv3) > 9999) {
      result = false;
      self.csv3Result = result;
      return 'trop_grand';
    } else if (isNaN(csv3)) {
      result = false;
      self.csv3Result = result;
      return 'invalid';
    } else if (this.phrases.some(e => e === ('CSV3_' + csv3))) {
      result = false;
      self.csv3Result = result;
      return 'existe';
    }*/
    self.csv3Result = true;
    return '';
  }

  openModal(content) {
    const self = this;
      self.modalService.open(content).result.then((result) => {
          self.closeResult = `Closed with: ${result}`;
      }, (reason) => {
          self.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return  `with: ${reason}`;
      }
  }
  public fileOverBase(e:any, zone:string):void {
      this.hasBaseDropZoneOver[zone] = e;
      // console.log(e);
  }
  onTxtDrop(e:any):void{
    //this.checkPdf(e, this.pdf);

    if(this.txt.queue.length > 1 )
    {
      this.txt.queue = this.txt.queue.slice(1);
    }
    if(this.txt.queue[0].file.type != "text/plain" || this.txtInput.nativeElement.files[0]){

        //this.openModal(this.pdfError);
        this.txt.removeFromQueue(this.txt.queue[0]);
        this.filename = true;

        //this.checkPdf(e, uploader);
      }
    else
    {
      this.filename = false;
      this.disabled = true;
      this.txtFileResult = true
    }
  }
  removeFile(item:any, uploader:FileUploader){

    uploader.removeFromQueue(item);
    this.disabled = false;
  }
}
