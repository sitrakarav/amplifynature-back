
import { Deserializable } from "./deserializable.model";
import { MotEnjeu } from "./motEnjeu.model";
export interface PerfArray{
  performance:number;
  enjeu:Enjeu;
}
export class LangEnjeu{
	en:MotEnjeu[];
	fr:MotEnjeu[];
}
export class Enjeu implements Deserializable{

	key:string;
	num:string;
	titre:string;
	pre_def:string;
	def:string;
	image_url:string;
	image_blanc_url:string;
	picto_png:string;
	picto_png_blanc:string;
	mots:LangEnjeu;
	extraits:number;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
