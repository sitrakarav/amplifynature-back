
import { Deserializable } from "./deserializable.model";
import { Enjeu } from "./enjeu.model";

export class Phrase implements Deserializable{

	num:string;
	key:string;
	langue:string;
	compteur_perf:string;
	enjeu:any = 0;
	satisfaction:number = 1;
	phrase_en:string;
	phrase_en_html:string;
	phrase_fr:string;
	phrase_fr_html:string;
	flux:string;
	csv_phrase_id:string;
	valid:boolean = true;

	deserialize(input: any) {
	    Object.assign(this, input);
	    if(this.satisfaction == 0){
	    	this.satisfaction = 1;
	    }
	    return this;
	}
}

