import { Injectable } from '@angular/core';
import { FileItem } from 'ng2-file-upload';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';

import { DropFileComponent } from '../../layout/drop-file/drop-file.component';

@Injectable({
  providedIn: 'root'
})
export class PdfService {

  constructor(
  	private storage:AngularFireStorage,
  	private db:AngularFireDatabase
  	) { }


  export(item:FileItem, page:DropFileComponent){
  	console.log("exporting %o", item);
  	let self = this;
    let name = item.file.name.toLowerCase();
    let file = item['_file'];


  	let ref = this.db.list('phrases').snapshotChanges().subscribe(function(snapshot:any){
      ref.unsubscribe();
      let found = snapshot.filter((item) => item.payload.val().nom_pdf.toLowerCase() == name.split('.')[0]);
      console.log("found 1 %o", found);
  		if(found.length > 0){
        let csv = found[0].payload.val();
  			// snapshot.forEach(function(childSnap){
  				page.loading = true;
  				page.pdfLoading = true;
  				self.storage.ref("pdf/"+name).put(file).then(function(storageSnap){
  					storageSnap.ref.getDownloadURL().then(value =>{
			            console.log(value);
			            self.db.database.ref('phrases/'+csv.nom+"/pdf_url/").set(value).then(function(){
			            	page.loading = false;
			            	page.pdfLoading = false;
			            	page.success.push("PDF "+name+" uploadé avec succès.");
			            	self.db.database.ref('pdf').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
			            
			            		
			            	
			            		
			            	}).catch(function(error){
			            		page.errors.push(error);
			            	});
			            	
			            })
			       
			        });
  				}).catch(function(error){
			        page.errors.push(error);
			    });
  			// });

  		}else{
        console.log("search in pdg");
        // self.db.database.ref('phrases').orderByChild("nom_pdg").equalTo(name.split('.')[0]).once('value', function(snapshot){
          found = snapshot.filter((item) => item.payload.val().nom_pdg ? item.payload.val().nom_pdg.toLowerCase() == name.split('.')[0] : false);
          console.log("found %o", found);
          if(found.length > 0){

            // snapshot.forEach(function(childSnap){
              let csv = found[0].payload.val();
              page.loading = true;
              page.pdfLoading = true;
              console.log("putting file");
              self.storage.ref("pdg/"+name).put(file).then(function(storageSnap){
                console.log("putted file");
                storageSnap.ref.getDownloadURL().then(value =>{
                      console.log("url file get");
                      self.db.database.ref('phrases/'+csv.nom+"/pdg_url/").set(value).then(function(){
                        page.loading = false;
                        page.pdfLoading = false;
                        page.success.push("Page de garde "+name+" uploadé avec succès.");
                        self.db.database.ref('pdf').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
                      
                          
                        
                          
                        }).catch(function(error){
                          page.errors.push(error);
                        });
                        
                      })
                 
                  });
              }).catch(function(error){
                  page.errors.push(error);
              });
            // });

          }else{
            console.log("ato am null");
            page.pdfLoading = false;
            page.loading = false;
            page.errors.push("Aucune référence ne correspond au fichier :"+name);
            console.log(page.errors);
          }
          if(item && item != undefined){
            try {
              page.pdf.removeFromQueue(item);
            }
            catch(error) {
            
            }
            
          } 
        // });
  			// page.enjeuPngLoading = false;
  			// page.loading = false;
  			// page.errors.push("Aucune référence ne correspond au pdf :"+name);
  		}
      if(item && item != undefined){
            try {
              page.pdf.removeFromQueue(item);
            }
            catch(error) {
            
            }
      } 
  	});

        
  }
 

  list(){
  	return this.db.list('pdf').snapshotChanges();
  }

  remove(item, page:DropFileComponent){
    let self = this;
    page.loading = true;
    if(item.name.toLowerCase().includes("pdf")){
      this.db.database.ref('pdf/'+item.name.split('.')[0]).remove().then(function(){
         page.loading = true;
        self.storage.ref('pdf/'+item.name).delete().subscribe(function(){
             page.loading = false;
             page.success.push('PDF '+ item.name+' supprimé avec succès.');
        });
      })
    }else{
      this.db.database.ref('pdf/'+item.name.split('.')[0]).remove().then(function(){
         page.loading = true;
        self.storage.ref('pdg/'+item.name).delete().subscribe(function(){
             page.loading = false;
             page.success.push('PDG '+ item.name+' supprimé avec succès.');
        });
      })
    }
      
  }
}
