import { Injectable } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { Client } from "../models/client.model";
import { Flux } from "../models/flux.model";
import { DropFileComponent } from '../../layout/drop-file/drop-file.component';

import { Papa } from 'ngx-papaparse';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(
  	private db: AngularFireDatabase,
  	public papa:Papa,
  	private storage: AngularFireStorage,
    private auth: AngularFireAuth
  	) { }

  public add(client:Client){
    let ext = [".png", ".svg", ".jpg", ".jpeg"];
      let cur = ext[0];
      let isBreak = false;
      for(let i in ext){
        console.log("checking type %o", ext[i]);
        let ref = this.storage.ref("clients/"+client.client_png+ext[i]);
        let ref2 = this.storage.ref("clients/"+client.frontoffice_png+ext[i]);
        let ref3 = this.storage.ref("clients/"+client.favicon_png+ext[i]);
        let ref4 = this.storage.ref("organisations/"+client.orga_png+ext[i]);
      
      
          ref.getDownloadURL().subscribe((value) =>{
            
            client.img_url = value;
            this.db.database.ref('clients').child(client.num).set(client);
            isBreak = true;
       
          });
          

        
       
          ref2.getDownloadURL().subscribe((value) =>{
      
            client.frontoffice_img_url = value;
            this.db.database.ref('clients').child(client.num).set(client);
            isBreak = true;

          });
        

     
       
          ref3.getDownloadURL().subscribe((value) =>{
       
            client.favicon_img_url = value;
            this.db.database.ref('clients').child(client.num).set(client);
            isBreak = true;
          });


        
        
          ref4.getDownloadURL().subscribe((value) =>{
      
            client.orga_img_url = value;
            this.db.database.ref('clients').child(client.num).set(client);
            isBreak = true;
          });

          if(isBreak){
            break;
          }


        

        
      }
  	 this.db.database.ref('clients').child(client.num).set(client);
     this.auth.auth.createUserWithEmailAndPassword(client.mail, client.password);

  }

  public list(){
  	return this.db.list("clients").snapshotChanges();
  }

  public getfile(){
  	return this.db.object('csv_client').snapshotChanges();
  }

  public remove(csvClient, page:DropFileComponent){
    let self = this;
    page.loading = true;
  	this.db.database.ref('csv_client/name').remove().then(function(){
      self.db.database.ref('csv_client').remove().then(function(){
          self.db.database.ref('clients').remove().then(function(){
            self.storage.ref('csv_client/'+csvClient.name).delete().subscribe(function(){
              page.loading = false;
              page.success.push("Fichier csv client supprimé avec succès.");
            })
          })
      })
          
    });
  }


  public export(file:File, name:string, page:DropFileComponent){

  	this.papa.parse(file, {
  			header: true,
            beforeFirstChunk: function(chunk) {
                    let rows = chunk.split( /\r\n|\r|\n/ );
                    let headings = rows[0].toLowerCase().replace(/ /g, '');;
                    console.log("HEADING phrase : %o", headings);
                    rows[0] = headings;
                    return rows.join("\r\n");
                },
            complete: (result) => {
                page.loading = true;
                page.clientLoading = true;

                let datas:any[] = this.check(result.data, page);
                if(datas.length > 0){
                	
                	let self = this;
                	this.db.database.ref('clients').remove().then(function(){
                			for (var i = 0; i < datas.length; ++i) {
		                		self.add(self.convertHeader(datas[i]));
		                	}
      						  	self.db.database.ref("csv_client").set({name: name}).then(function(){
      						  		self.storage.ref('csv_client/'+name).put(file).then(function(snapshot){
                          console.log("csv client put");
                          snapshot.ref.getDownloadURL().then(function(value){
                              self.db.database.ref("csv_client/url").set(value).then(function(){
                                  page.loading = false;
                                  page.clientLoading = false;
                                  page.success.push("Le fichier CSV Client "+name+ " a été uploadé avec succès.");
                              })
                          })
                              
      						  			
      						  			
      						  		});
      						  	})

                	});
		                	
                }else{
                	page.loading = false;
                	page.clientLoading = false;
                	console.log("Les données du fichier CSV Client ne sont pas conformes!");
                	// page.errors.push("Les données du fichier CSV Client ne sont pas conformes!");
                }

                page.client.clearQueue();
            }
        });
  }


  convertHeader(client:any):Client{
  	let res:Client = new Client();
  	// num client;prénom ;nom ;mail ;organisation ;poste;langue client;mdp ;orga png;client png;flux client 1;flux client 2;flux client 3;flux client 4;flux client 5;flux client 6;flux client 7;flux client 8

  	res.num = this.getCol(client, "num_client");
  	res.prenom = this.getCol(client, "prenom");
  	res.nom = this.getCol(client, "nom");
  	res.mail = this.getCol(client, "mail_login");
  	res.organisation = this.getCol(client, "organisation");
  	res.poste = this.getCol(client, "poste");
  	res.langue = this.getCol(client, "langue_client");
  	res.password = this.getCol(client, "mdp");
  	res.last_connection = this.getCol(client, "last_connection");
  	res.orga_img_url = this.getCol(client, "orga_png");
  	res.orga_png = this.getCol(client, "orga_png");
  	res.img_url = this.getCol(client, "client_png");
    res.client_png = this.getCol(client, "client_png");
    res.mots_cles = this.getCol(client, "mots_cles");

    res.favicon_png = this.getCol(client, "favicon_png");
  	res.frontoffice_png = this.getCol(client, "frontoffice_png");

  	let flux:Flux[]= [];
  	for (var i = 1; i < 31; ++i) {
  		let f = new Flux();
      if(client['flux_client_'+i]){
        f.flux = client['flux_client_'+i];
        flux.push(f);
      }else{
        break;
      }
  		
  	}

  	res.flux = flux;
  	return res;

  }

  public getCol(client:any, col:string){
  	return client[col] ? client[col] : "";
  }

  public check(datas, page:DropFileComponent){
  	let length = 0;
  	let res = [];
  	for (var i = 0; i < datas.length; ++i) {
  		// console.log(datas[i]);
  		// console.log(datas[i]['num_client']);
      let check = true;
      if(!datas[i]['num_client'] || datas[i]['num_client'] == ""){
        check = false;

        page.errors.push("Erreur lors de l'exportation du fichier CSV Client : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_client;prenom;nom;mail_login;organisation;poste;langue_client;mdp;last_connection;orga_png;client_png;mots_cles;flux_client_1;flux_client_2;flux_client_3;flux_client_4;flux_client_5;flux_client_6;flux_client_7;flux_client_8");
        page.errors.push("Erreur : Colonne <strong> num_client</strong>");
      }

      // let checkArray = ["prenom", 'nom', 'mail_login', 'organisation', 'poste','langue_client',
      //     'mdp',
      //     'last_connection',
      //     'orga_png',
      //     'client_png',
      //     'favicon_png',
      //     'frontoffice_png',
      //     'flux_client_1',
      //     'flux_client_2',
      //     'flux_client_3',
      //     'flux_client_4',
      //     'flux_client_5',
      //     'flux_client_6',
      //     'flux_client_7',
      //     'flux_client_8'
      //     ];
      let checkArray = ["prenom", 'nom', 'mail_login', 'organisation', 'poste','langue_client',
          'mdp',
          'last_connection',
          'orga_png',
          'client_png',
          'favicon_png',
          'frontoffice_png',
          'flux_client_1'
          ];

      for(let j in checkArray){

          if(datas[i][checkArray[j]] == undefined){
            check = false;

            page.errors.push("Erreur lors de l'exportation du fichier CSV Client : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_client;prenom;nom;mail_login;organisation;poste;langue_client;mdp;last_connection;orga_png;client_png;mots_cles;flux_client_1;flux_client_2;flux_client_3;flux_client_4;flux_client_5;flux_client_6;flux_client_7;flux_client_8");
            page.errors.push("Erreur. Colonne <strong> "+checkArray[j]+"</strong> : "+datas[i][checkArray[j]]);
          }
      }  

      if(check){
        res.push(datas[i]);
      }
      if(!check){
        continue;
      }
  	// 	if(!datas[i]['num_client'] || datas[i]['num_client'] == "" ||
  	// 		!datas[i]['prenom'] ||
  	// 		!datas[i]['nom'] ||
  	// 		!datas[i]['mail_login'] ||
  	// 		!datas[i]['organisation'] ||
  	// 		!datas[i]['poste'] ||
			// !datas[i]['langue_client'] ||
  	// 		!datas[i]['mdp'] ||
  	// 		!datas[i]['last_connection'] ||
  	// 		!datas[i]['orga_png'] ||
  	// 		!datas[i]['client_png'] ||
  	// 		!datas[i]['flux_client_1'] ||
  	// 		!datas[i]['flux_client_2'] ||
  	// 		!datas[i]['flux_client_3'] ||
  	// 		!datas[i]['flux_client_4'] ||
  	// 		!datas[i]['flux_client_5'] ||
  	// 		!datas[i]['flux_client_6'] ||
  	// 		!datas[i]['flux_client_7'] ||
  	// 		!datas[i]['flux_client_8']

  	// 		){
  	// 		if(res.length == 0){
  	// 			page.errors.push("Erreur lors de l'exportation du fichier CSV Client : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_client;prenom;nom;mail_login;organisation;poste;langue_client;mdp;last_connection;orga_png;client_png;flux_client_1;flux_client_2;flux_client_3;flux_client_4;flux_client_5;flux_client_6;flux_client_7;flux_client_8");
  	// 			break;
  	// 		}else{
  	// 			page.errors.push("Fichier CSV Client : La ligne "+Number(i+2)+" ne peut pas être exportée <br> "+JSON.stringify(datas[i]));
  	// 		}
  	// 		console.log("error in %o", datas[i]);
  			
  	// 	}else{
  	// 		res.push(datas[i]);
  	// 	}
  	}
  	return res;
  }

}
