import { Injectable,  NgZone } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { DropFileComponent } from '../../layout/drop-file/drop-file.component';
import { Papa } from 'ngx-papaparse';
import { Enjeu } from "../models/enjeu.model";
import { Client } from "../models/client.model";
import { LangEnjeu } from "../models/enjeu.model";
import { Phrase } from "../models/phrase.model";
import { MotEnjeu } from "../models/motEnjeu.model";

@Injectable({
  providedIn: 'root'
})
export class EnjeuService {

  constructor(
  	private db: AngularFireDatabase,
  	public papa:Papa,
  	private storage: AngularFireStorage,
  	public zone: NgZone

  	) { }

  	public export(file:File, name:string, page:DropFileComponent){
  		this.papa.parse(file, {
  			header: true,
  			beforeFirstChunk: function(chunk) {
                    let rows = chunk.split( /\r\n|\r|\n/ );
                    let headings = rows[0].toLowerCase().replace(/ /g, '');;
                    console.log("HEADING phrase : %o", headings);
                    rows[0] = headings;
                    return rows.join("\r\n");
                },
            complete: (result) => {
  				console.log("parsed enjeu %o", result)
                let datas:any[] = this.check(result.data, page);
                if(datas.length > 0){
                	page.loading = true;
			        page.enjeuLoading = true;
                	let self = this;
                	console.log("removing enjeu");

                	// delete clients enjeux
                 	this.db.database.ref("clients").once("value",(cltData) =>{
             			let prom = new Promise((resolve, reject) => {
             				let clts:Client[] = [];
             			

			    			cltData.forEach((clt) =>{
			    					let client:Client = new Client().deserialize(clt.val());
			    					client.key = clt.key;
				    				clts.push(client);


				    			});
			    			clts.forEach((clt, index, array) =>{
			    				this.db.database.ref("clients/"+clt.key+"/enjeu").remove();
			    				if(index === array.length -1) resolve();
			    			})


				    	})

			    		

			    		prom.then(() =>{

			    			this.db.database.ref('enjeu').remove().then(function(){
		                		console.log(" enjeu removed");
		                			// for (let i = 0; i < datas.length; ++i) {
				                	// 	self.add(self.convertHeader(datas[i]));
				                	// }
				                	self.addEnjeux(datas).then(function(enjeuxRes){
				                		// self.db.database.ref("enjeu").on("value", function(enjeuxData){
				                		// 	let enjeux = enjeuxData.val();
				                		// 	console.log("added enjeux %o", enjeuxData.val());
				                		// 	self.db.database.ref("phrases_csv").once("value", function(phraseSnap){
					                	// 		let phrases_csv = phraseSnap.val();
					                	// 		let csv_key = phraseSnap.key;
					                	// 		for(let i in phrases_csv){
					                	// 			for(let index in phrases_csv[i]){
					                	// 				let phrase = phrases_csv[i][index];

					                	// 				if(!phrase.valid){
					                	// 					let enjeu:Enjeu = self.phraseService.getMaxPerfPhrase(phrase, enjeux).enjeu;
					                	// 					console.log("enjeux %o", enjeux);
					                	// 					self.db.database.ref("phrases_csv/"+csv_key+"/"+index+"/enjeu").set(enjeu.num);
					                				
					                	// 				}
					                	// 			}
					                	// 		}
					       
					                	// 	});

				                		// })
					                		

				                		self.db.database.ref("csv_enjeu").set({name: name}).then(function(){
			      						  		self.storage.ref('csv_enjeu/'+name).put(file).then(function(snapshot){
						                        snapshot.ref.getDownloadURL().then(function(value){
						                              self.db.database.ref("csv_enjeu/url").set(value).then(function(){
						                              	console.log("FINISHEEEEEEEEEED");
						                              	self.zone.run(() =>{
						                              		page.loading = false;
						                                  	page.enjeuLoading = false;
						                                  	page.success.push("Le fichier CSV Enjeu "+name+ " a été uploadé avec succès.");
						                              
						                              	})
						                              		
						                                  
						                                  })
						                        })
			                              
			      						  			
			      						  			
			      						  	});
			      						})	
				                	})
									  	
		                	});
			    		});
                		
                	})
                		

		                	
		                	
                }else{
                	page.loading = false;
                	page.enjeuLoading = false;
         
                }

                page.enjeu.clearQueue();
            }
        });
  	}

  	async addEnjeux(datas:any[]){
  		let self = this;
  		const promises = datas.map(function(item){
  			return self.add(self.convertHeader(item));
  		})
  		await Promise.all(promises);
  	}


  	public remove(csvEnjeu, page:DropFileComponent){
	    let self = this;
	    page.loading = true;
	  	this.db.database.ref('csv_enjeu/name').remove().then(function(){
	      self.db.database.ref('csv_enjeu').remove().then(function(){
	          self.db.database.ref('enjeu').remove().then(function(){
	            self.storage.ref('csv_enjeu/'+csvEnjeu.name).delete().subscribe(function(){
	              page.loading = false;
	              page.success.push("Fichier csv enjeu supprimé avec succès.");
	            })
	          })
	      })
	          
	    });
	  }

  	public getfile(){
	  	return this.db.object('csv_enjeu').snapshotChanges();
	}

	public getAll(){
		return this.db.list('enjeu').snapshotChanges();
	}

	public getEnjeux(){
		return this.db.database.ref('enjeu');
	}

	public getExtraits(){
		return this.db.database.ref("enjeux_extraits");
	}

  	public add(enjeu:Enjeu){
  		console.log("addinfg %o", enjeu);
  		let ext = [".svg", ".png", ".jpg", ".jpeg"];
  		let cur = ext[0];
  		for(let i in ext){
  			let ref = this.storage.ref("enjeu/"+enjeu.picto_png+ext[i]);
  		
  	
  				ref.getDownloadURL().subscribe((value) =>{
  					console.log("picto found %o", value)
  					enjeu.image_url = value;
  					this.db.database.ref('enjeu').child(enjeu.num.replace(/#/g, '')).set(enjeu);
  				}, (err) =>{
  					console.log("not found");
  				});
  				break;
  			
  			
  		}
  		for(let i in ext){
  			let ref = this.storage.ref("enjeu/"+enjeu.picto_png_blanc+ext[i]);
  			console.log(ref.getDownloadURL());
  	
  				ref.getDownloadURL().subscribe((value) =>{
  					console.log("picto found %o", value)
  					enjeu.image_blanc_url = value;
  					this.db.database.ref('enjeu').child(enjeu.num.replace(/#/g, '')).set(enjeu);
  				});
  				break;
  			
  			
  		}
  		
  
  		return this.db.database.ref('enjeu').child(enjeu.num.replace(/#/g, '')).set(enjeu);

	  	
	}

	convertHeader(enjeu){
		let res:Enjeu = new Enjeu();
  	
	  	res.num = this.getCol(enjeu, "enjeu_num");
	  	res.titre = this.getCol(enjeu, "enjeu_titre");
	  	res.pre_def = this.getCol(enjeu, "enjeu_pre_def");
	  	res.def = this.getCol(enjeu, "enjeu_def");
	  	res.image_url = this.getCol(enjeu, "picto_png");
	  	res.picto_png = this.getCol(enjeu, "picto_png");
	  	res.picto_png_blanc = this.getCol(enjeu, "picto_png_blanc");
	  	let motEnjeu:LangEnjeu = new LangEnjeu();
	  	let fr:MotEnjeu[] = [];
	  	let en:MotEnjeu[] = [];
	  	for (let i = 1; i <= 10; ++i) {
	  		let mot = new MotEnjeu();
	  		mot.mot = enjeu['mot_'+i+'_fr_6_points'];
	  		mot.point = 6;
			fr.push(mot);
	  	}
	  	for (let i =11; i <= 35; ++i) {
	  		let mot = new MotEnjeu();
	  		if(enjeu['mot_'+i+'_fr_2_points'] != undefined && enjeu['mot_'+i+'_fr_2_points'] != ""){
	  			mot.mot = enjeu['mot_'+i+'_fr_2_points'];
		  		mot.point = 2;
				fr.push(mot);
	  		}else{
	  			break;
	  		}
		  				
	  	}

	  	for (let i = 1; i <= 10; ++i) {
	  		let mot = new MotEnjeu();
	  		mot.mot = enjeu['mot_'+i+'_en_6_points'];
	  		mot.point = 6;
			en.push(mot);
	  	}
	  	for (let i =11; i <= 35; ++i) {
	  		let mot = new MotEnjeu();
	  		if(enjeu['mot_'+i+'_en_2_points'] != undefined && enjeu['mot_'+i+'_en_2_points'] != ""){
	  			mot.mot = enjeu['mot_'+i+'_en_2_points'];
		  		mot.point = 2;
				en.push(mot);	
	  		}else{
	  			break;
	  		}
	  			
	  	}
	  	motEnjeu.en = en;
	  	motEnjeu.fr = fr;
	  	res.mots = motEnjeu;


	  	return res;
	  		
	}

	public getCol(enjeu:any, col:string){
	  	return enjeu[col] ? enjeu[col] : "";
	 }

  	check(datas, page:DropFileComponent){
  		let length = 0;
	  	let res = [];
	  	for (var i = 0; i < datas.length; ++i) {
	  		let ok = true;

	  		let checkArray = ['enjeu_titre', 'enjeu_pre_def', 'enjeu_def' ,'picto_png', "picto_png_blanc"];

	  		for(let j in checkArray){
	  			if(datas[i][checkArray[j]]  == undefined ){
	  			
	  				page.errors.push("Erreur lors de l'exportation du fichier CSV Enjeu : Ligne "+Number(i+2)+" <br> "
	  					+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> enjeu_num;enjeu_titre;enjeu_pre_def;enjeu_def;picto_png;picto_png_blanc,mot_1_FR_6_points;mot_2_FR_6_points;mot_3_FR_6_points;mot_4_FR_6_points;mot_5_FR_6_points;mot_6_FR_6_points;mot_7_FR_6_points;mot_8_FR_6_points;mot_9_FR_6_points;mot_10_FR_6_points;mot_11_FR_2_points;mot_12_FR_2_points;mot_13_FR_2_points;mot_14_FR_2_points;mot_15_FR_2_points;mot_16_FR_2_points;mot_17_FR_2_points;mot_18_FR_2_points;mot_19_FR_2_points;mot_20_FR_2_points;mot_21_FR_2_points;mot_22_FR_2_points;mot_23_FR_2_points;mot_24_FR_2_points;mot_25_FR_2_points;mot_26_FR_2_points;mot_27_FR_2_points;mot_28_FR_2_points;mot_29_FR_2_points;mot_30_FR_2_points;mot_1_EN_6_points;mot_2_EN_6_points;mot_3_EN_6_points;mot_4_EN_6_points;mot_5_EN_6_points;mot_6_EN_6_points;mot_7_EN_6_points;mot_8_EN_6_points;mot_9_EN_6_points;mot_10_EN_6_points;mot_11_EN_2_points;mot_12_EN_2_points;mot_13_EN_2_points;mot_14_EN_2_points;mot_15_EN_2_points;mot_16_EN_2_points;mot_17_EN_2_points;mot_18_EN_2_points;mot_19_EN_2_points;mot_20_EN_2_points;mot_21_EN_2_points;mot_22_EN_2_points;mot_23_EN_2_points;mot_24_EN_2_points;mot_25_EN_2_points;mot_26_EN_2_points;mot_27_EN_2_points;mot_28_EN_2_points;mot_29_EN_2_points;mot_30_EN_2_points"
	  					);

		  			page.errors.push("Erreur. Colonne <strong> "+checkArray[j]+"</strong> : "+datas[i][checkArray[j]]);

		  			
		  			ok = false;
	  			}
	  		}

	  		if(!datas[i]['enjeu_num'] || datas[i]['enjeu_num'] == ""
	  			){

	  			page.errors.push("Erreur lors de l'exportation du fichier CSV Enjeu : Ligne "+Number(i+2)+" <br> "
	  				// +JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> enjeu_num;enjeu_titre;enjeu_pre_def;enjeu_def;picto_png;mot_1_FR_6_points;mot_2_FR_6_points;mot_3_FR_6_points;mot_4_FR_6_points;mot_5_FR_6_points;mot_6_FR_6_points;mot_7_FR_6_points;mot_8_FR_6_points;mot_9_FR_6_points;mot_10_FR_6_points;mot_11_FR_2_points;mot_12_FR_2_points;mot_13_FR_2_points;mot_14_FR_2_points;mot_15_FR_2_points;mot_16_FR_2_points;mot_17_FR_2_points;mot_18_FR_2_points;mot_19_FR_2_points;mot_20_FR_2_points;mot_21_FR_2_points;mot_22_FR_2_points;mot_23_FR_2_points;mot_24_FR_2_points;mot_25_FR_2_points;mot_26_FR_2_points;mot_27_FR_2_points;mot_28_FR_2_points;mot_29_FR_2_points;mot_30_FR_2_points;mot_1_EN_6_points;mot_2_EN_6_points;mot_3_EN_6_points;mot_4_EN_6_points;mot_5_EN_6_points;mot_6_EN_6_points;mot_7_EN_6_points;mot_8_EN_6_points;mot_9_EN_6_points;mot_10_EN_6_points;mot_11_EN_2_points;mot_12_EN_2_points;mot_13_EN_2_points;mot_14_EN_2_points;mot_15_EN_2_points;mot_16_EN_2_points;mot_17_EN_2_points;mot_18_EN_2_points;mot_19_EN_2_points;mot_20_EN_2_points;mot_21_EN_2_points;mot_22_EN_2_points;mot_23_EN_2_points;mot_24_EN_2_points;mot_25_EN_2_points;mot_26_EN_2_points;mot_27_EN_2_points;mot_28_EN_2_points;mot_29_EN_2_points;mot_30_EN_2_points"
	  				);

	  			page.errors.push("Erreur. Colonne <strong> enjeu_num </strong> : "+datas[i]['enjeu_num']);

	  			
	  			ok = false;
	  			
	  		}
	  		
	  			
	  			for (var j = 1; j <= 10; ++j) {
	  				if(datas[i]['mot_'+j+'_fr_6_points'] == undefined || datas[i]['mot_'+j+'_fr_6_points'] == null){
	  					ok = false;
	  					page.errors.push("Erreur lors de l'exportation du fichier CSV Enjeu : Ligne "+Number(i+2)+" <br> ");
	  					page.errors.push("Erreur. Colonne <strong> "+'mot_'+j+'_fr_6_points' +" </strong> : "+datas[i]['mot_'+j+'_fr_6_points']);

	  					console.log("error in fr 6 %o %o",j, datas[i])
	  				}
	  			}
	  			for (var j =11; j <= 30; ++j) {
	  				if(datas[i]['mot_'+j+'_fr_2_points'] == undefined || datas[i]['mot_'+j+'_fr_2_points'] == null){
	  					ok = false;
	  					page.errors.push("Erreur lors de l'exportation du fichier CSV Enjeu : Ligne "+Number(i+2)+" <br> ");
	  					page.errors.push("Erreur. Colonne <strong> "+ 'mot_'+j+'_fr_2_points' +" </strong> : "+datas[i]['mot_'+j+'_fr_2_points']);
	  					console.log("error in fr 2 %o %o",j, datas[i])
	  					console.log("error in fr 2 %o %o",j, datas[i]['mot_'+j+'_fr_2_points'])
	  				}
	  			}
	  			for (var j = 1; j <= 10; ++j) {
	  				if(datas[i]['mot_'+j+'_en_6_points'] == undefined || datas[i]['mot_'+j+'_en_6_points'] == null){
	  					ok = false;
	  					page.errors.push("Erreur lors de l'exportation du fichier CSV Enjeu : Ligne "+Number(i+2)+" <br> ");
	  					page.errors.push("Erreur. Colonne <strong> "+'mot_'+j+'_en_6_points' +" </strong> : "+datas[i]['mot_'+j+'_en_6_points']);
	  					console.log("error in en 6 %o %o",j, datas[i])
	  				}
	  			}
	  			for (var j =11; j <= 30; ++j) {
	  				if(datas[i]['mot_'+j+'_en_2_points'] == undefined || datas[i]['mot_'+j+'_en_2_points'] == null){
	  					ok = false;
	  					page.errors.push("Erreur lors de l'exportation du fichier CSV Enjeu : Ligne "+Number(i+2)+" <br> ");
	  					page.errors.push("Erreur. Colonne <strong> "+"mot_"+j+"_en_2_points" +" </strong> : "+datas[i]['mot_'+j+'_en_2_points']);
	  					console.log("error in en 2 %o %o",j, datas[i])
	  				}
	  			}
	  
	  			
	  		
	  		if(!ok){
	  			continue;
	  		}
	  		// if(!ok && res.length == 0){
	  		// 		page.errors.push("Erreur lors de l'exportation du fichier CSV Enjeu : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> enjeu_num;enjeu_titre;enjeu_pre_def;enjeu_def;picto_png;mot_1_FR_6_points;mot_2_FR_6_points;mot_3_FR_6_points;mot_4_FR_6_points;mot_5_FR_6_points;mot_6_FR_6_points;mot_7_FR_6_points;mot_8_FR_6_points;mot_9_FR_6_points;mot_10_FR_6_points;mot_11_FR_2_points;mot_12_FR_2_points;mot_13_FR_2_points;mot_14_FR_2_points;mot_15_FR_2_points;mot_16_FR_2_points;mot_17_FR_2_points;mot_18_FR_2_points;mot_19_FR_2_points;mot_20_FR_2_points;mot_21_FR_2_points;mot_22_FR_2_points;mot_23_FR_2_points;mot_24_FR_2_points;mot_25_FR_2_points;mot_26_FR_2_points;mot_27_FR_2_points;mot_28_FR_2_points;mot_29_FR_2_points;mot_30_FR_2_points;mot_1_EN_6_points;mot_2_EN_6_points;mot_3_EN_6_points;mot_4_EN_6_points;mot_5_EN_6_points;mot_6_EN_6_points;mot_7_EN_6_points;mot_8_EN_6_points;mot_9_EN_6_points;mot_10_EN_6_points;mot_11_EN_2_points;mot_12_EN_2_points;mot_13_EN_2_points;mot_14_EN_2_points;mot_15_EN_2_points;mot_16_EN_2_points;mot_17_EN_2_points;mot_18_EN_2_points;mot_19_EN_2_points;mot_20_EN_2_points;mot_21_EN_2_points;mot_22_EN_2_points;mot_23_EN_2_points;mot_24_EN_2_points;mot_25_EN_2_points;mot_26_EN_2_points;mot_27_EN_2_points;mot_28_EN_2_points;mot_29_EN_2_points;mot_30_EN_2_points");
	  		// 		break;
	  		// } 

	  		// if(!ok && res.length > 0){
	  		// 		page.errors.push("Fichier CSV Enjeu : La ligne "+Number(i+2)+" ne peut pas être exportée <br> "+JSON.stringify(datas[i]));
	  		// }

	  		if(ok){
	  			res.push(datas[i]);
	  		}
	  	}
	  	return res;
  	}
}
