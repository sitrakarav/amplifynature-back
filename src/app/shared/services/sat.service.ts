import { Injectable } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { DropFileComponent } from '../../layout/drop-file/drop-file.component';
import { Papa } from 'ngx-papaparse';
import { Enjeu } from "../models/enjeu.model";
import { LangEnjeu } from "../models/enjeu.model";
import { Phrase } from "../models/phrase.model";
import { MotEnjeu } from "../models/motEnjeu.model";

@Injectable({
  providedIn: 'root'
})
export class SatService {

  constructor(
  	private db: AngularFireDatabase,
  	public papa:Papa,
  	private storage: AngularFireStorage

  	) { }

  	public export(file:File, name:string, page:DropFileComponent){
  		this.papa.parse(file, {
  			header: true,
  			delimiter: ";",
  			beforeFirstChunk: function(chunk) {
                    let rows = chunk.split( /\r\n|\r|\n/ );
                    let headings = rows[0].toLowerCase().replace(/ /g, '');;
                    console.log("HEADING phrase : %o", headings);
                    rows[0] = headings;
                    return rows.join("\r\n");
                },
            complete: (result) => {
 
                let datas:any[] = this.check(result.data, page);
                if(datas.length > 0){
                	page.loading = true;
			        page.satLoading = true;
                	let self = this;
                	this.db.database.ref('sat').remove().then(function(){
   	
		                	self.addSat(datas).then(function(enjeuxRes){
		                			

		                		self.db.database.ref("csv_sat").set({name: name}).then(function(){
	      						  		self.storage.ref('csv_sat/'+name).put(file).then(function(snapshot){
				                        snapshot.ref.getDownloadURL().then(function(value){
				                              self.db.database.ref("csv_sat/url").set(value).then(function(){
				                                  page.loading = false;
				                                  page.satLoading = false;
				                                  page.success.push("Le fichier CSV Satisfaction "+name+ " a été uploadé avec succès.");
				                              })
				                        })
	                              
	      						  			
	      						  			
	      						  	});
	      						})	
		                	})
							  	
                	});
		                	
                }else{
                	page.loading = false;
                	page.satLoading = false;
         
                }

                page.sat.clearQueue();
            }
        });
  	}

  	addSat(datas:any[]){
  		let self = this;
  		let resFr =[];
  		let resEn =[];

  		for(let d in datas){
  			let item = datas[d];

  			// for(let i in item){

  				if(!resFr.includes(item["satisf_1"].trim()) && item['satisf_1'].trim() != ""){
  					
		  			resFr.push(item["satisf_1"].trim());
		  			
		  		}

		  		if(!resEn.includes(item["satisf_1_en"].trim()) && item['satisf_1_en'].trim() != ""){
		  			
		  			resEn.push(item["satisf_1_en"].trim());
		  			
		  		}
  			// }
  		}

  		let promises = [];
  		promises.push(this.db.database.ref('sat/fr').set(resFr));
  		promises.push(this.db.database.ref('sat/en').set(resEn));
  		

	  	return 	Promise.all(promises);
  		
  		// const promises = datas.map(function(item){
  		// 	return self.add(self.convertHeader(item));
  		// })
  		// await Promise.all(promises);
  	}


  	public remove(csvSat, page:DropFileComponent){
	    let self = this;
	    page.loading = true;
	  	this.db.database.ref('csv_sat/name').remove().then(function(){
	      self.db.database.ref('csv_sat').remove().then(function(){
	          self.db.database.ref('sat').remove().then(function(){
	            self.storage.ref('csv_sat/'+csvSat.name).delete().subscribe(function(){
	              page.loading = false;
	              page.success.push("Fichier CSV Satisfaction supprimé avec succès.");
	            })
	          })
	      })
	          
	    });
	  }

  	public getfile(){
	  	return this.db.object('csv_sat').snapshotChanges();
	}

	public getAll(){
		return this.db.list('sat').snapshotChanges();
	}

	public getSat(){
		return this.db.database.ref('sat');
	}



  	public add(sat){

  		return this.db.database.ref('sat').push(sat);
	}

	convertHeader(data){
		let res = [];

	  	for(let i in data){
	  		
	  		if(!res.includes(data[i].trim())){
	  			res.push(data[i].trim())
	  		}
	  		
	  	}

	  	return res;		
	}

	public getCol(sat:any, col:string){
	  	return sat[col] ? sat[col] : "";
	 }

  	check(datas, page:DropFileComponent){
  		let length = 0;
	  	let res = [];
	  	for (var i = 0; i < datas.length; ++i) {
	  		let ok = true;

	  		let checkArray = ['satisf_1', 'satisf_1_en'];


	  		for(let j in checkArray){
	  			if(datas[i][checkArray[j]]  == undefined ){	

	  					ok = false;
	  					page.errors.push("Erreur lors de l'exportation du fichier CSV Satisfaction : Ligne "+Number(i+2)+" <br> ");
	  					page.errors.push("Erreur. Colonne <strong> "+checkArray[j]+" </strong> : "+ JSON.stringify(datas[i]));

	  					
	  				}
	  		
	  		}	
	  			
	  		
	  		if(!ok){
	  			continue;
	  		}


	  		if(ok){
	  			res.push(datas[i]);
	  		}
	  	}
	  	return res;
  	}
}
