import { Injectable } from '@angular/core';
import { FileItem } from 'ng2-file-upload';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';

import { DropFileComponent } from '../../layout/drop-file/drop-file.component';

@Injectable({
  providedIn: 'root'
})
export class EnjeuPngService {

  constructor(
  	private storage:AngularFireStorage,
  	private db:AngularFireDatabase
  	) { }


  export(item:FileItem, page:DropFileComponent){
  	console.log("exporting %o", item);
  	let self = this;
    let name = item.file.name;
    let file = item['_file'];
    let found = false;
    page.enjeuPngLoading = true;
    page.loading = true;

    let promises = [];
    promises.push(
      this.db.database.ref('enjeu').orderByChild("picto_png").equalTo(name.split('.')[0]).once('value', function(snapshot){
        if(snapshot.val()){
          self.storage.ref("enjeu/"+name).put(file).then(function(storageSnap){
              storageSnap.ref.getDownloadURL().then(value =>{
                      found = true;
                        snapshot.forEach(function(childSnap){
                          page.enjeuPngLoading = true;
                          page.loading = true;
                          
                                  console.log(value);
                                  self.db.database.ref('enjeu/'+childSnap.val().num.replace(/#/g, '')+"/image_url/").set(value).then(function(){
                                    page.enjeuPngLoading = false;
                                    page.loading = false;
                                    page.success.push("Png enjeu "+name+" uploadé avec succès.");
                                    self.db.database.ref('enjeuPng').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
                                      
                                    
                                      
                                    }).catch(function(error){
                                      page.errors.push(error);
                                    });
                                    
                                  })
                             



                             
                        });
              });
          }).catch(function(error){
                page.errors.push(error);
          });    
                        

        }else{
          page.enjeuPngLoading = false;
          page.loading = false;

          
        }
        page.enjeuPng.removeFromQueue(item);
      })
    )
    	

    promises.push(
      this.db.database.ref('enjeu').orderByChild("picto_png_blanc").equalTo(name.split('.')[0]).once('value', function(snapshot){
      if(snapshot.val()){
        self.storage.ref("enjeu/"+name).put(file).then(function(storageSnap){
            storageSnap.ref.getDownloadURL().then(value =>{
                    snapshot.forEach(function(childSnap){
                      page.enjeuPngLoading = true;
                      page.loading = true;
                      
                              console.log(value);
                              self.db.database.ref('enjeu/'+childSnap.val().num.replace(/#/g, '')+"/image_blanc_url/").set(value).then(function(){
                                page.enjeuPngLoading = false;
                                page.loading = false;
                                page.success.push("Png enjeu "+name+" uploadé avec succès.");
                                self.db.database.ref('enjeuPng').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
                                  
                                
                                  
                                }).catch(function(error){
                                  page.errors.push(error);
                                });
                                
                              })
                         
                         
                    });
             });
        }).catch(function(error){
              page.errors.push(error);
        });
                    

        }else{
          page.enjeuPngLoading = false;
          page.loading = false;
          
        }
        page.enjeuPng.removeFromQueue(item);
      })
    );
    return Promise.all(promises);
  }
 

  list(){
  	return this.db.list('enjeuPng').snapshotChanges();
  }

  remove(item, page:DropFileComponent){
    let self = this;
    page.loading = true;
    this.db.database.ref('enjeuPng/'+item.name.split('.')[0]).remove().then(function(){
    	 page.loading = true;
      self.storage.ref('enjeu/'+item.name).delete().subscribe(function(){
           page.loading = false;
           page.success.push('Png enjeu '+ item.name+' supprimé avec succès.');
      });
    })
  }
}
