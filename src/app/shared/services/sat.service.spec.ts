import { TestBed } from '@angular/core/testing';

import { SatService } from './sat.service';

describe('SatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SatService = TestBed.get(SatService);
    expect(service).toBeTruthy();
  });
});
