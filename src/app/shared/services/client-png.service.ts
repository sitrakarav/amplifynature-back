import { Injectable } from '@angular/core';
import { FileItem } from 'ng2-file-upload';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';

import { DropFileComponent } from '../../layout/drop-file/drop-file.component';

@Injectable({
  providedIn: 'root'
})
export class ClientPngService {

  constructor(
  	private storage:AngularFireStorage,
  	private db:AngularFireDatabase
  	) { }


  export(item:FileItem, page:DropFileComponent){
  	console.log("exporting %o", item);
  	let self = this;
    let name = item.file.name;
    let file = item['_file'];
  	this.db.database.ref('clients').orderByChild("client_png").equalTo(name.split('.')[0]).once('value', function(snapshot){
  		if(snapshot.val()){
        self.storage.ref("clients/"+name).put(file).then(function(storageSnap){
            storageSnap.ref.getDownloadURL().then(value =>{
                    snapshot.forEach(function(childSnap){
                      page.clientPngLoading = true;
                      page.loading = true;
                      
                              console.log(value);
                              self.db.database.ref('clients/'+childSnap.val().num+"/img_url/").set(value).then(function(){
                                page.clientPngLoading = false;
                                page.loading = false;
                                page.success.push("Png client "+name+" uploadé avec succès.");
                                self.db.database.ref('clientsPng').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
                                  
                                
                                  
                                }).catch(function(error){
                                  page.errors.push(error);
                                });;
                                
                              })
                         

                    });
            });
        }).catch(function(error){
              page.errors.push(error);
        });
              			

  		}else{
        // flavicon
        self.db.database.ref('clients').orderByChild("favicon_png").equalTo(name.split('.')[0]).once('value', function(snapshot){
          if(snapshot.val()){
            self.storage.ref("clients/"+name).put(file).then(function(storageSnap){
              storageSnap.ref.getDownloadURL().then(value =>{
                      snapshot.forEach(function(childSnap){
                      page.clientPngLoading = true;
                      page.loading = true;
                      
                              console.log(value);
                              self.db.database.ref('clients/'+childSnap.val().num+"/favicon_img_url/").set(value).then(function(){
                                page.clientPngLoading = false;
                                page.loading = false;
                                page.success.push("Png client "+name+" uploadé avec succès.");
                                self.db.database.ref('clientsPng').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
                                  
                                
                                  
                                }).catch(function(error){
                                  page.errors.push(error);
                                });;
                                
                              })
                         
                      });
              });
            }).catch(function(error){
                  page.errors.push(error);
            });

                    

          }else{
              // frontoffice
              console.log("frontoffice");
              self.db.database.ref('clients').orderByChild("frontoffice_png").equalTo(name.split('.')[0]).once('value', function(snapshot){
                console.log("found frontoffice_png %o", snapshot.val());
                if(snapshot.val()){
                  console.log("found frontoffice_png %o", snapshot.val());
                  console.log("putting image %o", file);
                  self.storage.ref("clients/"+name).put(file).then(function(storageSnap){
                  
                    storageSnap.ref.getDownloadURL().then(value =>{
                            console.log("image put %o", value);

                            snapshot.forEach(function(childSnap){
                            page.clientPngLoading = true;
                            page.loading = true;

                                    console.log("URL FRONTOFFICE %o",value);
                                    self.db.database.ref('clients/'+childSnap.val().num+"/frontoffice_img_url/").set(value).then(function(){
                                      page.clientPngLoading = false;
                                      page.loading = false;
                                      page.success.push("Png client "+name+" uploadé avec succès.");
                                      self.db.database.ref('clientsPng').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
                                        
                                      
                                        
                                      }).catch(function(error){
                                        page.errors.push(error);
                                      });;
                                      
                                    })
                               


                          });
                    });
                  }).catch(function(error){
                        page.errors.push(error);
                  });
                          

                }else{
                  page.clientPngLoading = false;
                  page.loading = false;
                  page.errors.push("Aucune référence ne correspond au png client :"+name);
                }

                if(item) page.clientPng.removeFromQueue(item);
              });
          }
          if(item) page.clientPng.removeFromQueue(item);
        });

  			// page.errors.push("Aucune référence ne correspond au png client :"+name);
  		}
      if(item) page.clientPng.removeFromQueue(item);
  	});

        

               
  }
 

  list(){
  	return this.db.list('clientsPng').snapshotChanges();
  }

  remove(item, page:DropFileComponent){
    let self = this;
    page.loading = true;
    this.db.database.ref('clientsPng/'+item.name.split('.')[0]).remove().then(function(){
      self.storage.ref('clients/'+item.name).delete().subscribe(function(){
           page.loading = false;
           page.success.push('Png client '+ item.name+' supprimé avec succès.');
      });
    })
  }
}
