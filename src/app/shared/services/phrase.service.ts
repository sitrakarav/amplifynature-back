import { Injectable } from '@angular/core';
import { FileItem } from 'ng2-file-upload';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { DropFileComponent } from '../../layout/drop-file/drop-file.component';
import { PhraseComponent } from '../../layout/phrase/phrase.component';
import { Papa } from 'ngx-papaparse';
import { Phrase } from "../models/phrase.model";
import { Enjeu } from "../models/enjeu.model";
import { CsvPhrase } from "../models/csv-phrase.model";
import { EnjeuService } from "./enjeu.service";

@Injectable({
  providedIn: 'root'
})
export class PhraseService {

	enjeux:Enjeu[];

  constructor(
  	private db: AngularFireDatabase,
  	public papa:Papa,
  	private storage: AngularFireStorage,
  	public enjeuService:EnjeuService

  	) { }

  	public export(item:FileItem, page:DropFileComponent){
  		let file = item['_file'];
  		let name = item.file.name;


  		return this.papa.parse(file, {
  			header: true,
  			delimiter: ";",
  			beforeFirstChunk: function(chunk) {
                    let rows = chunk.split( /\r\n|\r|\n/ );
                    let headings = rows[0].toLowerCase().replace(/ /g, '');;
                    console.log("HEADING phrase : %o", headings);
                    rows[0] = headings;
                    return rows.join("\r\n");
                },
            complete: (result) => {
  				// console.log("parsed phrase %o", result);
  				page.phraseLoading = true;
  				page.loading = true;

                let datas:any[] = this.check(result.data, name, page);
                if(datas.length > 0){

                	let self = this;

                			self.enjeuService.getAll().subscribe(function(data2){

			                    let enjeux = data2.map(a => ({ key: a.key, ...a.payload.val() }));
			                    self.enjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));
			                    let csvPhrase = self.convertHeader(datas);
			                	self.add(csvPhrase).then(function(){
			                		// self.db.database.ref("phrases_csv/"+csvPhrase.nom).remove().then(function(){
			                			self.addPhrases(csvPhrase, self.getPhrases(datas)).then(function(){
				                				self.db.database.ref("csv_phrase").child(name.split('.')[0]).set({name: name}).then(function(){
											  		self.storage.ref('csv_phrase/'+name).put(file).then(function(storageSnap){
											  			storageSnap.ref.getDownloadURL().then(function(value){
											  				self.db.database.ref("csv_phrase").child(name.split('.')[0]+'/url').set(value).then(function(){
											  					page.loading = false;
													  			page.phraseLoading = false;

													  			page.success.push("Le fichier CSV Phrase "+name+ " a été exporté avec succès.");
											  				})
											  			})

											  		}).catch(function(error){
											  			console.log(error);
								                		page.errors.push(error);
								                	});
											  	}).catch(function(error){
											  		console.log(error);
								                		page.errors.push(error);
								                	});
				                		})
			                		// })



			                	}).catch(function(error){
			                		console.log(error);
			                		page.errors.push(error);
			                	})
			                })






                }else{
                	page.loading = false;
                	console.log("Les données du fichier CSV Client ne sont pas conformes!");
                	// page.errors.push("Les données du fichier CSV Client ne sont pas conformes!");
                }

                page.phrase.clearQueue();
            }
        });
  	}

  	public list(){
	  	return this.db.database.ref('csv_phrase');
	  }
    public listPhrases(){
      return this.db.database.ref('phrases');
    }

	public allPhrases(){
		return this.db.database.ref('phrases_csv');
	}

  	public add(csvPhrase:CsvPhrase){
  		// console.log("adding csvPhrase %o", csvPhrase);
  		csvPhrase.nom_pdf = csvPhrase.nom_pdf.toLowerCase();
  		csvPhrase.nom_pdg = csvPhrase.nom_pdg.toLowerCase();

  		let self = this;
  		let promises = [];
  		

  		promises.push(this.db.database.ref('phrases').child(csvPhrase.nom).set(csvPhrase));
  		promises.push(this.db.database.ref('csv_phrase').child(csvPhrase.nom).set({name : csvPhrase.nom+".csv"}));
  		promises.push(
  			this.storage.ref("pdf/"+csvPhrase.nom_pdf+".pdf").getDownloadURL().subscribe(function(value){
  				if(value){
  					csvPhrase.pdf_url = value;
  					self.db.database.ref('phrases').child(csvPhrase.nom).set(csvPhrase);
  				}
  			})
  		);

  		promises.push(
  			this.storage.ref("pdg/"+csvPhrase.nom_pdg+".png").getDownloadURL().subscribe(function(value){
  				if(value){
  					csvPhrase.pdg_url = value;
  					self.db.database.ref('phrases').child(csvPhrase.nom).set(csvPhrase);
  				}
  			})
  		);
	  	return Promise.all(promises);;

	}

	public addPhrases(csvPhrase:CsvPhrase, phrases: Phrase[]){
		 console.log("ADD PHRASES %o", phrases);

		return this.db.database.ref("phrases_csv").child(csvPhrase.nom).set(phrases);
	}



	getPhrasesByCsv(csvPhrase:string){
		return this.db.object("phrases_csv/"+csvPhrase).snapshotChanges();
	}

	public removePhrases(){
		console.log("deleting all phrases");

		return this.db.database.ref("phrases_csv").remove();
	}

	public update(csvPhrase:CsvPhrase,phrase:Phrase,i, enjeux: Enjeu[]){

  		let self = this;
  		this.enjeux = enjeux;
  		// this.db.database.ref('phrases_csv/'+csvPhrase.nom+"/"+i+"/satisfaction").once("value", function(snap){
  		// 	if(snap.val()){
  		// 		let sat  = parseInt(snap.val());
  		// 		if(phrase.satisfaction != sat){
  		// 			self.db.database.ref('phrases/'+csvPhrase.nom+"/sat/"+phrase.satisfaction).once("value", function(phraseSnap){
				//   		if(phraseSnap.val()){
				//   			let val = parseInt(phraseSnap.val()) +1;

				//   			self.db.database.ref('phrases/'+csvPhrase.nom+"/sat/"+phrase.satisfaction).set(val);
				//   		}else{
				//   			self.db.database.ref('phrases/'+csvPhrase.nom+"/sat/"+phrase.satisfaction).set(1);
				//   		}
				//   	});
  		// 			//diminuer l'ancien sat
				//   	self.db.database.ref('phrases/'+csvPhrase.nom+"/sat/"+sat).once("value", function(phraseSnap2){
				//   		if(phraseSnap2.val()){
				//   			let val = ((parseInt(phraseSnap2.val()) -1) > 0 ) ? (parseInt(phraseSnap2.val()) -1) : 0;
				//   			self.db.database.ref('phrases/'+csvPhrase.nom+"/sat/"+phrase.satisfaction).set(val);
				//   		}
				//   	});

  		// 		}
  		// 	}else{

  		// 	}

  		// })


		this.db.database.ref('phrases_csv/'+csvPhrase.nom+"/"+i).update({
		  		"valid": true,
		  		"enjeu": phrase.enjeu,
		  		"compteur_perf": phrase.compteur_perf,
		  		"satisfaction" :  phrase.satisfaction
		}).then(() =>{
			self.getSeuil().once("value", (snapSeuil) => {
				let seuil = 0
				if(snapSeuil.val()){
					seuil = snapSeuil.val();
				}
				let ref = self.getPhrasesByCsv(csvPhrase.nom).subscribe(function(phrasesSnap){
                                    ref.unsubscribe()

                                    let phrases = phrasesSnap.payload.val();
                                    let sat = [];
                                    let enjeux_extraits = [];




                                    	for(let p in phrases){

	                                    	let phrase = new Phrase().deserialize(phrases[p]);
	                                    	let res = self.getMaxPerfPhrase(phrase);
	                                    	let perf = res.perf;
	                                    	let enjeu = res.enjeu;

	                            			if(enjeu){

	                            				let enj = enjeu.num.replace(/#/g, '');
	                            				if(enjeux_extraits[enj] == undefined){
	                            					enjeux_extraits[enj] = 1;
	                            				}else{
	                            					enjeux_extraits[enj] +=1;
	                            				}

		                            			if(sat[enj] == undefined){
		                            				sat[enj] = [];
		                            			}
		                                    	phrase.key = p;
		                                    	if(perf >= seuil){
													if(sat[enj][phrase.satisfaction] == undefined ){
			                                    		sat[enj][phrase.satisfaction] = 1;
			                                    	}else{
			                                    		sat[enj][phrase.satisfaction] += 1;
			                                    	}
												}
	                            			}


	                                    }

	                                for(let e in enjeux_extraits){
	                                	 self.db.database.ref('enjeux_extraits/'+e+"/"+csvPhrase.nom).set(enjeux_extraits[e]);
	                                }

                                    self.db.database.ref('phrases/'+csvPhrase.nom+"/sat/").set(sat);

            	});
			})


		});
	}


	public getAll(){
		return this.db.list('csv_phrase').snapshotChanges();
	}

	public getWithPhrases(){
		return this.db.database.ref('phrases');
	}

	public getSeuil(){
		return this.db.database.ref("seuil");
	}

	public setSeuil(seuil:number){
		return this.db.object("seuil").set(seuil);
		// return this.db.database.ref("seuil").set(seuil);
	}

	getCsv(name){
		return this.db.database.ref('phrases/'+name);
	}

	public setEnjeu(phrase:Phrase,index, enjeu:Enjeu){

		return this.db.database.ref("phrases_csv/"+phrase.csv_phrase_id+"/"+index+"/enjeu").set(enjeu.num);
	}

	validate(phrase){
		return this.db.database.ref("csv_phrase/"+phrase.name+"/valid").set(true);
	}

	remove(item, page:DropFileComponent){
	    let self = this;
	    page.loading = true;
	    this.db.database.ref('csv_phrase/'+item.name.split('.')[0]).remove().then(function(){
	    	self.db.database.ref('phrases/'+item.name.split('.')[0]).remove().then(function(){
	    		self.db.database.ref('phrases_csv/'+item.name.split('.')[0]).remove().then(function(){
	    			self.storage.ref('csv_phrase/'+item.name).delete().subscribe(function(){
				           page.loading = false;
				           page.success.push('Csv Phrase '+ item.name+' supprimé avec succès.');
				    });
	    		})

	    	})

	    })
	}

	convertHeader(datas){
		let res:CsvPhrase = new CsvPhrase();
		// let phrases:Phrase[] = [];
  		for (let i = 0; i < datas.length; ++i) {
  			let csvPhrase = datas[i];


  				res.nom = this.getCol(csvPhrase, "nom_csv");
			  	res.nom_pdf = this.getCol(csvPhrase, "nom_pdf");
			  	res.nom_pdf = res.nom_pdf.toLowerCase();
			  	res.nom_pdg = this.getCol(csvPhrase, "nom_pdg");
			  	res.nom_pdg = res.nom_pdg.toUpperCase();
			  	res.titre_fr = this.getCol(csvPhrase, "titre_fr");
			  	res.titre_en = this.getCol(csvPhrase, "titre_en");
			  	res.auteur = this.getCol(csvPhrase, "auteur");
			  	res.date = this.getCol(csvPhrase, "date");
			  	res.pays = this.getCol(csvPhrase, "pays");
			  	res.long = this.getCol(csvPhrase, "coord_gps_x");
			  	res.lat = this.getCol(csvPhrase, "coord_gps_y");
			  	res.flux = this.getCol(csvPhrase, "flux_appartenance");
			  	res.sat =[ true, false, false, false];
			  	res.extraits = datas.length;

			  	break

  		}

	  	return res;
	}

	getPhrases(datas){
		let self = this;
		let phrases:Phrase[] = [];
		let extraits = [];
  		for (let i = 0; i < datas.length; ++i) {
  			let csvPhrase = datas[i];

  				let phrase = new Phrase();
  				phrase.num = this.getCol(csvPhrase, "num_phrase");
  				phrase.langue = this.getCol(csvPhrase, "langue_phrase");
  				phrase.compteur_perf = this.getCol(csvPhrase, "compteur_perf");
  				phrase.enjeu = this.getCol(csvPhrase, "enjeu_a_valider");
  				phrase.phrase_fr = this.getCol(csvPhrase, "phrase_fr");
  				phrase.phrase_en = this.getCol(csvPhrase, "phrase_en");
  				phrase.flux = this.getCol(csvPhrase, "flux_appartenance");
  				phrase.csv_phrase_id = this.getCol(csvPhrase, "nom_csv");

  				let enjeu = this.getMaxPerfPhrase(phrase).enjeu;


  				let perf = this.getMaxPerfPhrase(phrase).perf;
  				if(enjeu){
  					let enjeu_id = enjeu.num.replace(/#/g, '');

  					phrase.enjeu = enjeu.num;
  					phrase.compteur_perf = perf.toString();
  					if(!(enjeu_id in extraits)){
  						extraits[enjeu_id] = 1;
  					}else{
  						extraits[enjeu_id]+=1;
  					}
  				}


  				phrases.push(phrase);

  		}
  		let csv = this.getCol(datas[0], "nom_csv");

  		const entries = Object.entries(extraits);
  		console.log("extraits %o", entries);
  		for (const [key, value] of entries) {
		  this.db.database.ref("enjeux_extraits/"+key+"/"+csv).set(value);
		}

  		return phrases;
	}

	public setEnjeux(enjeux:Enjeu[]){
		this.enjeux = enjeux;
	}

	public setEnjeuxExtraits(csvPhrase: CsvPhrase, seuil:Number, satPhrases:{}){
		let extraits = {};

		let sat = [];
		let allSat = {};

		for(let enj of this.enjeux){
			allSat[enj.key] = {};
			sat[enj.key] = {};	
			
			extraits[enj.key] =  csvPhrase.phrases.filter((phrase) =>{
					return phrase.enjeu == enj.num && Number(phrase.compteur_perf) > seuil;
			}).length;

			for(let i of [1,2,3,4]){
				allSat[enj.key][i] = csvPhrase.phrases.filter((phrase) =>{
					return phrase.enjeu == enj.num && phrase.satisfaction == i;
				}).length;


				sat[enj.key][i] = csvPhrase.phrases.filter((phrase) =>{
					return phrase.enjeu == enj.num && phrase.satisfaction == i  && Number(phrase.compteur_perf) > seuil;
				}).length;
			}
 		}

 		for(let i of [1,2,3,4]){
 			if(satPhrases[i] == undefined){
 				satPhrases[i] = 0;
 			}
 			satPhrases[i] += csvPhrase.phrases.filter((phrase) =>{
				return phrase.satisfaction == i && Number(phrase.compteur_perf) > seuil;
			}).length;
 		}



		csvPhrase.sat = sat;

		
		// if(csvPhrase.nom_pdf){
		// 	csvPhrase.nom_pdf = csvPhrase.nom_pdf.toLowerCase();
		// 	this.db.database.ref("phrases/"+csvPhrase.nom+"/nom_pdf").set(csvPhrase.nom_pdf);
		// }

		// if(csvPhrase.nom_pdg){
		// 	csvPhrase.nom_pdg = csvPhrase.nom_pdg.toLowerCase();
		// 	this.db.database.ref("phrases/"+csvPhrase.nom+"/nom_pdg").set(csvPhrase.nom_pdg);
		// } 

		this.db.database.ref("phrases/"+csvPhrase.nom+"/sat").set(sat);
		this.db.database.ref("phrases/"+csvPhrase.nom+"/allSat").set(allSat);


		this.db.database.ref("phrases_csv/"+csvPhrase.nom).set(csvPhrase.phrases);
		let csv = csvPhrase.nom;

		const entries = Object.entries(extraits);


  		for (const [key, value] of entries) {
			this.db.database.ref("enjeux_extraits/"+key+"/"+csv).set(value);
		}



		return satPhrases;
	}

	public getCol(csvPhrase:any, col:string){
	  	return csvPhrase[col] ? csvPhrase[col] : "";
	}

	getMaxPerfPhrase(phrase:Phrase){
	    // console.log("getting max perf");

	    let enjeu:Enjeu;
	    let perf = 0;

	    for(let i in this.enjeux){

	      let cur = this.computePerf(phrase, this.enjeux[i]);

	      if(cur > perf){
	        enjeu = this.enjeux[i];
	        perf = cur;
	      }
	    }

	    return {"perf": perf, "enjeu": enjeu};
	}

	computePerf(phrase:Phrase, enjeu:Enjeu){
    //mettre phrase en minuscule
    if(phrase){
        let phrase_fr = " "+phrase.phrase_fr.toLowerCase()+" ";
        let phrase_en = " "+phrase.phrase_en.toLowerCase()+" ";

        let perf_fr = 0;
        let perf_en = 0;
        //fr
        for(let i in enjeu.mots.fr){
          let reg = new RegExp("\\s" + enjeu.mots.fr[i].mot.toLowerCase() + "\\s");
          if(phrase_fr.search(reg) > 0){
            perf_fr += enjeu.mots.fr[i].point;

          }
        }

        //en
        for(let i in enjeu.mots.en){
          let reg = new RegExp("\\s" + enjeu.mots.en[i].mot.toLowerCase() + "\\s");
          if(phrase_en.search(reg) > 0){
            perf_en += enjeu.mots.en[i].point;

          }
        }

        return (phrase.langue.toLowerCase() == "fr") ? perf_fr : perf_en;
    }else{
      return 0;
    }


  }

  	check(datas, nom_csv:string, page:DropFileComponent){
  		let length = 0;
	  	let res = [];



	  	let checkArray = ['langue_phrase', 'compteur_perf','enjeu_a_valider', 'satisfaction_a_valider', 'nom_csv', 'nom_pdf', 'nom_pdg', 'phrase_fr', 'phrase_en', 'flux_appartenance', 'titre_fr', 'titre_en', 'auteur', 'date', 'pays', 'coord_gps_x', 'coord_gps_x'];

	  	let errors = "";
	  	for (var i = 0; i < datas.length; ++i) {

	  		let ok =true;

	  		if(datas[i]['nom_csv'] !== nom_csv.split('.')[0]){
	  				page.errors.push("CSV : "+nom_csv+". <strong>La colonne nom_csv devrait être le nom du fichier</strong> : ");
	  				break;
	  		}
	  		if(datas[i]['num_phrase'] == undefined || datas[i]['num_phrase'] == ""){

	  				console.log("erreur csv %o",datas[i]);
	  				// page.errors.push("Ligne "+Number(i+2)+"La colonne nom_csv est le nom du fichier </strong>"
	  				// 	);
	  				// errors +="CSV : "+nom_csv+". Ligne "+Number(i+2)+". Erreur. Colonne <strong> num_phrase </strong> : "+datas[i]['num_phrase']+"<br/>";
		  			page.errors.push("CSV : "+nom_csv+". Ligne "+Number(i+2)+". Colonne <strong> num_phrase </strong> : "+datas[i]['num_phrase']);


		  			ok = false;

	  		}
	  		for(let j in checkArray){
	  			if(datas[i][checkArray[j]] == undefined  ){
	  				console.log("erreur csv %o",datas[i]);
	  				// page.errors.push("Ligne "+Number(i+2)+"La colonne nom_csv est le nom du fichier </strong>"
	  				// 	);
	  				// errors +="CSV : "+nom_csv+". Ligne "+Number(i+2)+". Erreur. Colonne <strong> "+checkArray[j]+"</strong> : "+datas[i][checkArray[j]]+"<br/>";
		  			page.errors.push("CSV : "+nom_csv+". Ligne "+Number(i+2)+". Colonne <strong> "+checkArray[j]+"</strong> : "+datas[i][checkArray[j]]);


		  			ok = false;
	  			}
	  		}
	  		// page.errors.push(errors)
	  		// if(datas[i]['num_phrase'] == undefined || datas[i]['num_phrase'] == "" ||
	  		// 	datas[i]['langue_phrase'] == undefined ||


	  		// 	datas[i]['compteur_perf'] == undefined ||
	  		// 	datas[i]['enjeu_a_valider'] == undefined ||
	  		// 	datas[i]['satisfaction_a_valider'] == undefined ||

	  		// 	datas[i]['nom_csv'] == undefined ||
	  		// 	datas[i]['nom_pdf'] == undefined ||

	  		// 	datas[i]['phrase_fr'] == undefined ||
	  		// 	datas[i]['phrase_en'] == undefined ||

	  		// 	datas[i]['flux_appartenance'] == undefined ||
	  		// 	datas[i]['titre_fr'] == undefined ||
	  		// 	datas[i]['titre_en'] == undefined ||
	  		// 	datas[i]['auteur'] == undefined ||
	  		// 	datas[i]['date'] == undefined ||
	  		// 	datas[i]['pays'] == undefined ||
	  		// 	datas[i]['coord_gps_x'] == undefined ||
	  		// 	datas[i]['coord_gps_y'] == undefined

	  		// 	){


	  		// 	if(res.length == 0){

	  		// 		page.errors.push("Erreur lors de l'exportation du fichier CSV Phrase : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y <br> <strong> La colonne nom_csv est le nom du fichier </strong> ");
	  		// 		break;
	  		// 	}else{
	  		// 		page.errors.push("Fichier CSV Phrase : La ligne "+Number(i+2)+" ne peut pas être exportée <br> "+JSON.stringify(datas[i])+"<br> <strong> La colonne nom_csv est le nom du fichier </strong>");
	  		// 	}
	  		// 	// console.log("error in %o", datas[i]);

	  		// }else{
	  		// 	if(datas[i]['nom_csv'] !== nom_csv.split('.')[0] && res.length == 0){
	  		// 		page.errors.push("Erreur lors de l'exportation du fichier CSV Phrase : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y <br> <strong> La colonne nom_csv est le nom du fichier </strong> ");
	  		// 		break;
	  		// 	}else{
	  		// 		res.push(datas[i]);
	  		// 	}

	  		// }

	  		if(!ok){
	  			continue;
	  		}
			if(ok){
	  			res.push(datas[i]);
	  		}
	  	}
	  	return res;
  	}


}
