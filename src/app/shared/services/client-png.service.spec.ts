import { TestBed } from '@angular/core/testing';

import { ClientPngService } from './client-png.service';

describe('ClientPngService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientPngService = TestBed.get(ClientPngService);
    expect(service).toBeTruthy();
  });
});
