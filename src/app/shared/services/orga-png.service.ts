import { Injectable } from '@angular/core';
import { FileItem } from 'ng2-file-upload';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';

import { DropFileComponent } from '../../layout/drop-file/drop-file.component';

@Injectable({
  providedIn: 'root'
})
export class OrgaPngService {

  constructor(
  	private storage:AngularFireStorage,
  	private db:AngularFireDatabase
  	) { }


  export(item:FileItem, page:DropFileComponent){
  	let self = this;
    let name = item.file.name;
    let file = item['_file'];
  	this.db.database.ref('clients').orderByChild("orga_png").equalTo(name.split('.')[0]).once('value', function(snapshot){
  		if(snapshot.val()){
        self.storage.ref("organisations/"+name).put(file).then(function(storageSnap){
            storageSnap.ref.getDownloadURL().then(value =>{
                    snapshot.forEach(function(childSnap){
                    page.orgaPngLoading = true;
                    page.loading = true;
                    
                            console.log(value);
                            self.db.database.ref('clients/'+childSnap.val().num+"/orga_img_url/").set(value).then(function(){
                              page.orgaPngLoading = false;
                              page.loading = false;
                              page.success.push("Png organisation "+name+" uploadé avec succès.");
                              self.db.database.ref('orgaPng').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
                                
                              
                                
                              }).catch(function(error){
                                page.errors.push(error);
                              });;
                              
                            })
                       
                        
                    });
            });
        }).catch(function(error){
                      page.errors.push(error);
        });
            			  

  		}else{
  			page.orgaPngLoading = false;
  			page.loading = false;
  			page.errors.push("Aucune référence ne correspond au png organisation :"+name);
  		}
      page.orga.removeFromQueue(item);
  	})
  }
 

  list(){
  	return this.db.list('orgaPng').snapshotChanges();
  }

  remove(item, page:DropFileComponent){
    let self = this;
    page.loading = true;
    this.db.database.ref('orgaPng/'+item.name.split('.')[0]).remove().then(function(){
      self.storage.ref('organisations/'+item.name).delete().subscribe(function(){
           page.loading = false;
           page.success.push('Png organisation '+ item.name+' supprimé avec succès.');
      });
    })
  }
}
