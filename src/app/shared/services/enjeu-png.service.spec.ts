import { TestBed } from '@angular/core/testing';

import { EnjeuPngService } from './enjeu-png.service';

describe('EnjeuPngService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnjeuPngService = TestBed.get(EnjeuPngService);
    expect(service).toBeTruthy();
  });
});
